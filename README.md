# Introduction
The data explosion phenomenon in modern applications causes tremendous stress on storage systems. Developers use data compression, a size-reduction technique, to address this issue. However, each compression library exhibits different strengths and weaknesses when considering the input data type and format. We present Ares, an intelligent, adaptive, and flexible compression framework which can dynamically choose a compression library for a given input data based on the type of workload and provides an appropriate infrastructure to users to fine-tune the chosen library. Ares is a modular framework which unifies several compression libraries while allowing the addition of more compression libraries by the user. Ares is a unified compression engine that abstracts the complexity of using different compression libraries for each workload. Evaluation results show that under real-world applications, from both scientific and Cloud domains, Ares performed 2-6x faster than competitive solutions with a low cost of additional data analysis (i.e., overheads around 10%) and up to 10x faster against a baseline of no compression at all.
# Who uses
This library is used within Hermes to perform compression on buffered data based on various application characteristics. 
# How it works

 - Ares contains a library (libares) which exposes API’s (compress and decompress).
 - Ares contains a CLI tool which can compress/decompress files and folders.
# Dependencies 
- JRE > 1.8
- Cmake > 3.5
- C++ > 11

  
# Installation
## Cmake
`mkdir build && cd build`
`cmake ../`
`make -j8 && make install`
# Usage
## Library
Suitable for applications that want to simply link and use the Ares on compile time directly. Think of a user-facing application which want to compress and decompress data buffers using C++ API.
###  C++ API
```cpp
/* buffer to buffer*/
bool ares_compress(const void * source, size_t source_size, void *& destination, size_t &destination_size, AresFlags &flags);
bool ares_decompress(const void *source, size_t source_size, void *&destination, size_t &destination_size, AresFlags &flags);

/* buffer to file*/
bool ares_compress(const void * source, size_t source_size, std::string, AresFlags &flags);
bool ares_decompress(std::string,void *&destination, size_t &destination_size, AresFlags &flags);

/* file/dir to file/dir*/
bool ares_compress(std::string, AresFlags &flags);
bool ares_decompress(std::string,AresFlags &flags);
```
### C API
```c
/* buffer to buffer*/
bool ares_compress(void * source, long unsigned int source_size, void * destination, long unsigned int destination_size, uint16_t data_type, uint16_t library,uint16_t format,uint16_t priority);
bool ares_compress(void * source, long unsigned int source_size, void * destination, long unsigned int destination_size, uint16_t data_type, uint16_t library,uint16_t format,uint16_t priority);

/* buffer to file*/
bool ares_compress(void * source, long unsigned int source_size, void * destination, long unsigned int destination_size, uint16_t data_type, uint16_t library,uint16_t format,uint16_t priority);
bool ares_compress(void * source, long unsigned int source_size, void * destination, long unsigned int destination_size, uint16_t data_type, uint16_t library,uint16_t format,uint16_t priority);

/* file/dir to file/dir*/
bool ares_compress(void * source, long unsigned int source_size, void * destination, long unsigned int destination_size, uint16_t data_type, uint16_t library,uint16_t format,uint16_t priority);
bool ares_compress(void * source, long unsigned int source_size, void * destination, long unsigned int destination_size, uint16_t data_type, uint16_t library,uint16_t format,uint16_t priority);
```
### JAVA
```java
byte[] Compress(byte[] source, int sourceSize, short data_type,short library, short format, short priority);
byte[] Compress(byte[] source, int sourceSize, short data_type,short library, short format, short priority);
```

## Map-Reduce Compressor
We have a Ares Compressor/Decompressor classes build within the ares_java.jar. This jar can be included in a map-reduce program and the compressor/decompressor can be added like a normal compression library. Internally, the map-reduce programs will utilize the Ares compression.

### Map-Reduce
```java
job.getConfiguration().setBoolean("mapreduce.map.output.compress", true);
job.getConfiguration().setInt("io.compression.codec.ares.priority", 1);
job.getConfiguration().setClass("mapred.map.output.compression.codec", AresCodec.class, CompressionCodec.class);
```
## CLI tool
Suitable for user who want to use Ares on the command-line interface and compress/decompress files and folders. From the CLI tool users can directly run Ares engine or even select specific compression libraries to compress/decompression.


```
Usage: ares_tool [Options]
Options:
		"-f <path>        file/directory to compress/decompress
		-c                Compress
		-d            Decompress
		-F <data-format>    Format of Data
		-L <library>        library to use (optional)
							1 - ares (default)
							2 - bz2
							3 - zlib
							4 - huff
							5 - sf
							6 - rice
							7 - rle
							8 - trle
							9 - lzo
							10 - pithy
							11 - snappy
							12 - lz
```
# Running Tests
## Ctest
### VPIC:
`ctest -R vpic`
### HACC:
`ctest -R hacc`
### Hadoop sort:
`ctest -R sort`
### Word Count:
`ctest -R wc`
### All:
`ctest`

# Publications:
- Devarajan, Hariharan, Anthony Kougkas, and Xian-He Sun. "An Intelligent, Adaptive, and Flexible Data Compression Framework." In 2019 19th IEEE/ACM International Symposium on Cluster, Cloud and Grid Computing (CCGRID), pp. 82-91. 2019.
# License:
# Acknowledgments:
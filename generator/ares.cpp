/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 9/19/18.
//

#include <algorithm>
#include "common/data_structure.h"
#include "common/utils.h"
#include "common/constants.h"
#include <cstdio>
#include <H5Ipublic.h>
#include <hdf5.h>
#include <netcdf.h>
#include <fstream>

#include "avro/avro_int.hh"
#include "avro/avro_char.hh"
#include "avro/avro_float.hh"
#include "avro/avro_double.hh"
#include "avro/Encoder.hh"
#include "avro/Decoder.hh"
#include "avro/ValidSchema.hh"
#include "avro/Compiler.hh"
#include "avro/DataFile.hh"

#include <arrow/api.h>
#include <arrow/io/api.h>
#include <parquet/arrow/writer.h>
#include <parquet/exception.h>

#include <mpi.h>

const std::string generator_dir="/home/hariharan/projects/ares/generator/";

void generate_file(InputArgs type);

bool handle_binary_files(InputArgs args);

bool handle_hdf5(InputArgs args);

bool handle_mpiio(InputArgs args);

bool handle_pnetcdf(InputArgs args);

bool handle_csv(InputArgs args);

bool handle_avro(InputArgs args);

bool handle_parquet(InputArgs args);

bool handle_xml(InputArgs args);

bool handle_json(InputArgs args);

void *generate_data(InputArgs args);

int main(int argc, char* argv[]) {
    InputArgs args;
    parse_opts(argc,argv,args);
    MPI_Init(&argc,&argv);
    std::vector<DataType> dataTypes=std::vector<DataType>();
    std::vector<DataFormat> dataformats=std::vector<DataFormat>();
    if(args.dataType==DataType::DUMMY){
        dataTypes.push_back(*DataType::CHAR);
        dataTypes.push_back(*DataType::SINT_SORTED);
        dataTypes.push_back(*DataType::SINT_UNSORTED);
        dataTypes.push_back(*DataType::UINT_SORTED);
        dataTypes.push_back(*DataType::UINT_UNSORTED);
        dataTypes.push_back(*DataType::FLOAT);
        dataTypes.push_back(*DataType::DOUBLE);
    }else{
        dataTypes.push_back(args.dataType);
    }
    if(args.format==DataFormat::DUMMY){
        dataformats.push_back(DataFormat::BINARY);
        dataformats.push_back(DataFormat::MPIIO);
        dataformats.push_back(DataFormat::HDF5);
        dataformats.push_back(DataFormat::NETCDF);
        dataformats.push_back(DataFormat::CSV);
        dataformats.push_back(DataFormat::JSON);
        dataformats.push_back(DataFormat::XML);
        dataformats.push_back(DataFormat::PARQUET);
        dataformats.push_back(DataFormat::AVRO);
    }else{
        dataformats.push_back(args.format);
    }

    for(auto dataType:dataTypes){
        for(auto dataFormat:dataformats){
            std::cout<<"format :"<<static_cast<std::underlying_type<DataFormat>::type>(dataFormat)<<" datatype:"<<dataType.index_<<std::endl;
            args.format=dataFormat;
            args.dataType=dataType;
            generate_file(args);
        }
    }
    MPI_Finalize();
    return 0;
}

void generate_file(InputArgs args) {
    switch(args.format){
        case DataFormat::BINARY:{
            handle_binary_files(args);
            break;
        }
        case DataFormat::HDF5:{
            handle_hdf5(args);
            break;
        }
        case DataFormat::MPIIO:{
            handle_mpiio(args);
            break;
        }
        case DataFormat::NETCDF:{
            handle_pnetcdf(args);
            break;
        }
        case DataFormat::CSV:{
            handle_csv(args);
            break;
        }
        case DataFormat::JSON:{
            handle_json(args);
            break;
        }
        case DataFormat::XML:{
            handle_xml(args);
            break;
        }
        case DataFormat::PARQUET:{
            handle_parquet(args);
            break;
        }
        case DataFormat::AVRO:{
            handle_avro(args);
            break;
        }
    }
}

bool handle_json(InputArgs args) {
    std::string filename=args.dir+"json_"+std::to_string(args.dataType.index_)+".json";
    size_t num_elements=args.size_mb_*MB/args.dataType.size_;
    void* buffer = generate_data(args);

    std::ofstream fs;
    fs.open(filename);
    std::string final_str="{'main':[";

    switch (args.dataType.index_) {
        case 1: {
            char* c = reinterpret_cast<char*>(buffer);
            for(int i=0;i<num_elements;i++) {
                if(i<num_elements-1)
                    final_str.append("'"+std::to_string(c[i])+",");
                else final_str.append("'"+c[i]);
            }
            break;
        }
        case 2:
        case 3: {
            int* c = reinterpret_cast<int*>(buffer);
            for(int i=0;i<num_elements;i++) {
                if(i<num_elements-1) final_str.append("'"+std::to_string(c[i])+",");
                else final_str.append("'"+std::to_string(c[i]));
            }
            break;
        }
        case 4:
        case 5: {
            uint * c = reinterpret_cast<uint*>(buffer);
            for(int i=0;i<num_elements;i++) {
                if(i<num_elements-1) final_str.append("'"+std::to_string(c[i])+",");
                else final_str.append("'"+std::to_string(c[i]));
            }
            break;
        }
        case 6: {
            float * c = reinterpret_cast<float*>(buffer);
            for(int i=0;i<num_elements;i++) {
                if(i<num_elements-1) final_str.append("'"+std::to_string(c[i])+",");
                else final_str.append("'"+std::to_string(c[i]));
            }
            break;
        }
        case 7: {
            double * c = reinterpret_cast<double*>(buffer);
            for(int i=0;i<num_elements;i++) {
                if(i<num_elements-1) final_str.append("'"+std::to_string(c[i])+",");
                else final_str.append("'"+std::to_string(c[i]));
            }
            break;
        }
    }
    final_str=final_str+"]}";

    fs <<final_str<<std::endl;
    fs.close();
    free(buffer);
    return SUCCESS_STATUS;
}

bool handle_xml(InputArgs args) {
    std::string filename=args.dir+"xml_"+std::to_string(args.dataType.index_)+".xml";
    size_t num_elements=args.size_mb_*MB/args.dataType.size_;
    void* buffer = generate_data(args);

    std::ofstream fs;
    fs.open(filename);
    std::string final_str= "<main>";

    switch (args.dataType.index_) {
        case 1: {
            char* c = reinterpret_cast<char*>(buffer);
            for(int i=0;i<num_elements;i++) {
                final_str.append(std::string("<item>")+ c[i]+"</item>");
            }
            break;
        }
        case 2:
        case 3: {
            int* c = reinterpret_cast<int*>(buffer);
            for(int i=0;i<num_elements;i++) {

                final_str.append("<item>"+ std::to_string(c[i])+"</item>");
            }
            break;
        }
        case 4:
        case 5: {
            uint * c = reinterpret_cast<uint*>(buffer);
            for(int i=0;i<num_elements;i++) {

                final_str.append("<item>"+ std::to_string(c[i])+"</item>");
            }
            break;
        }
        case 6: {
            float * c = reinterpret_cast<float*>(buffer);
            for(int i=0;i<num_elements;i++) {

                final_str.append("<item>"+ std::to_string(c[i])+"</item>");
            }
            break;
        }
        case 7: {
            double * c = reinterpret_cast<double*>(buffer);
            for(int i=0;i<num_elements;i++) {

                final_str.append("<item>"+ std::to_string(c[i])+"</item>");
            }
            break;
        }
    }

    final_str.append("</main>");
    fs <<final_str;
    fs.close();
    free(buffer);
    return SUCCESS_STATUS;

}

bool handle_parquet(InputArgs args) {
    std::string filename=args.dir+"parquet_"+std::to_string(args.dataType.index_)+".parquet";
    size_t num_elements=args.size_mb_*MB/args.dataType.size_;
    void* buffer = generate_data(args);
    remove(filename.c_str());
    switch (args.dataType.index_) {
        case 1: {
            std::shared_ptr<arrow::Array> strarray=std::shared_ptr<arrow::Array>();
            char *c = reinterpret_cast<char*>(buffer);
            arrow::StringBuilder strbuilder;
            strbuilder.Append(c);

            strbuilder.Finish(&strarray);
            std::shared_ptr<arrow::Schema> schema = arrow::schema(
                    {arrow::field("str", arrow::utf8())});
            std::shared_ptr<arrow::Table> table=arrow::Table::Make(schema, {strarray});
            std::shared_ptr<arrow::io::FileOutputStream> outfile;
            arrow::io::FileOutputStream::Open(filename, &outfile);
            parquet::arrow::WriteTable(table.operator*(), arrow::default_memory_pool(), outfile, num_elements);
            break;
        }
        case 2:
        case 3: {

            std::shared_ptr<arrow::Array> strarray=std::shared_ptr<arrow::Array>();
            int *c = reinterpret_cast<int*>(buffer);
            arrow::Int32Builder strbuilder;
            for(int i=0;i<num_elements;++i){
                strbuilder.Append(c[i]);
            }
            strbuilder.Finish(&strarray);
            std::shared_ptr<arrow::Schema> schema = arrow::schema(
                    {arrow::field("int", arrow::int32())});
            std::shared_ptr<arrow::Table> table=arrow::Table::Make(schema, {strarray});
            std::shared_ptr<arrow::io::FileOutputStream> outfile;
            arrow::io::FileOutputStream::Open(filename, &outfile);
            parquet::arrow::WriteTable(table.operator*(), arrow::default_memory_pool(), outfile, num_elements);
            break;
        }
        case 4:
        case 5: {
            std::shared_ptr<arrow::Array> strarray=std::shared_ptr<arrow::Array>();
            uint *c = reinterpret_cast<uint*>(buffer);
            arrow::UInt32Builder strbuilder;
            for(int i=0;i<num_elements;++i){
                strbuilder.Append(c[i]);
            }
            strbuilder.Finish(&strarray);
            std::shared_ptr<arrow::Schema> schema = arrow::schema(
                    {arrow::field("int", arrow::uint32())});
            std::shared_ptr<arrow::Table> table=arrow::Table::Make(schema, {strarray});
            std::shared_ptr<arrow::io::FileOutputStream> outfile;
            arrow::io::FileOutputStream::Open(filename, &outfile);
            parquet::arrow::WriteTable(table.operator*(), arrow::default_memory_pool(), outfile, num_elements);
            break;
        }
        case 6: {
            std::shared_ptr<arrow::Array> strarray=std::shared_ptr<arrow::Array>();
            float *c = reinterpret_cast<float*>(buffer);
            arrow::FloatBuilder strbuilder;
            for(int i=0;i<num_elements;++i){
                strbuilder.Append(c[i]);
            }
            strbuilder.Finish(&strarray);
            std::shared_ptr<arrow::Schema> schema = arrow::schema(
                    {arrow::field("float", arrow::float32())});
            std::shared_ptr<arrow::Table> table=arrow::Table::Make(schema, {strarray});
            std::shared_ptr<arrow::io::FileOutputStream> outfile;
            arrow::io::FileOutputStream::Open(filename, &outfile);
            parquet::arrow::WriteTable(table.operator*(), arrow::default_memory_pool(), outfile, num_elements);
            break;
        }
        case 7: {
            std::shared_ptr<arrow::Array> strarray=std::shared_ptr<arrow::Array>();
            double *c = reinterpret_cast<double*>(buffer);
            arrow::DoubleBuilder strbuilder;
            for(int i=0;i<num_elements;++i){
                strbuilder.Append(c[i]);
            }
            strbuilder.Finish(&strarray);
            std::shared_ptr<arrow::Schema> schema = arrow::schema(
                    {arrow::field("double", arrow::float64())});
            std::shared_ptr<arrow::Table> table=arrow::Table::Make(schema, {strarray});
            std::shared_ptr<arrow::io::FileOutputStream> outfile;
            arrow::io::FileOutputStream::Open(filename, &outfile);
            parquet::arrow::WriteTable(table.operator*(), arrow::default_memory_pool(), outfile, num_elements);
            break;
        }
    }
    free(buffer);
    return SUCCESS_STATUS;

}

bool handle_avro(InputArgs args) {
    std::string filename=args.dir+"avro_"+std::to_string(args.dataType.index_)+".avro";
    size_t num_elements=args.size_mb_*MB/args.dataType.size_;
    void* buffer = generate_data(args);
    std::string schema_file;
    switch (args.dataType.index_) {
        case 1: {
            schema_file=generator_dir+"schema/avro/char.json";
            std::ifstream ifs(schema_file);
            avro::ValidSchema result;
            avro::compileJsonSchema(ifs, result);

            {
                avro::DataFileWriter<avro_char> dfw(filename.c_str(), result);
                avro_char c1;
                uint8_t *c = reinterpret_cast<uint8_t*>(buffer);
                for(int i=0;i<num_elements;i++) {
                    c1.data = std::vector<uint8_t>();
                    c1.data.push_back(c[i]);
                    dfw.write(c1);
                }
                dfw.close();
            }
            break;
        }
        case 2:
        case 3: {
            schema_file=generator_dir+"schema/avro/integer.json";
            std::ifstream ifs(schema_file);
            avro::ValidSchema result;
            avro::compileJsonSchema(ifs, result);

            {
                avro::DataFileWriter<avro_int> dfw(filename.c_str(), result);
                avro_int c1;
                int *c = reinterpret_cast<int*>(buffer);
                for(int i=0;i<num_elements;i++) {
                    c1.data = c[i];
                    dfw.write(c1);
                }
                dfw.close();
            }
            break;
        }
        case 4:
        case 5: {
            schema_file=generator_dir+"schema/avro/integer.json";
            std::ifstream ifs(schema_file);
            avro::ValidSchema result;
            avro::compileJsonSchema(ifs, result);

            {
                avro::DataFileWriter<avro_int> dfw(filename.c_str(), result);
                avro_int c1;
                uint *c = reinterpret_cast<uint*>(buffer);
                for(int i=0;i<num_elements;i++) {
                    c1.data = static_cast<uint>(c[i]);
                    dfw.write(c1);
                }
                dfw.close();
            }
            break;
        }
        case 6: {
            schema_file=generator_dir+"schema/avro/float.json";
            std::ifstream ifs(schema_file);
            avro::ValidSchema result;
            avro::compileJsonSchema(ifs, result);

            {
                avro::DataFileWriter<avro_float> dfw(filename.c_str(), result);
                avro_float c1;
                float *c = reinterpret_cast<float*>(buffer);
                for(int i=0;i<num_elements;i++) {
                    c1.data = c[i];
                    dfw.write(c1);
                }
                dfw.close();
            }
            break;
        }
        case 7: {
            schema_file=generator_dir+"schema/avro/double.json";
            std::ifstream ifs(schema_file);
            avro::ValidSchema result;
            avro::compileJsonSchema(ifs, result);

            {
                avro::DataFileWriter<avro_double> dfw(filename.c_str(), result);
                avro_double c1;
                double *c = reinterpret_cast<double*>(buffer);
                for(int i=0;i<num_elements;i++) {
                    c1.data = c[i];
                    dfw.write(c1);
                }
                dfw.close();
            }
            break;
        }
    }
    free(buffer);
    return SUCCESS_STATUS;
}

bool handle_csv(InputArgs args) {
    std::string filename=args.dir+"csv_"+std::to_string(args.dataType.index_)+".txt";
    size_t num_elements=args.size_mb_*MB/args.dataType.size_;
    void* buffer = generate_data(args);

    std::ofstream fs;
    fs.open(filename);
    std::string final_str="";

    switch (args.dataType.index_) {
        case 1: {
            char* c = reinterpret_cast<char*>(buffer);
            for(int i=0;i<num_elements;i++) {
                final_str.append(c[i]+"");
            }
            break;
        }
        case 2:
        case 3: {
            int* c = reinterpret_cast<int*>(buffer);
            for(int i=0;i<num_elements;i++) {
                final_str.append(std::to_string(c[i])+"\n");
            }
            break;
        }
        case 4:
        case 5: {
            uint * c = reinterpret_cast<uint*>(buffer);
            for(int i=0;i<num_elements;i++) {
                final_str.append(std::to_string(c[i])+"\n");
            }
            break;
        }
        case 6: {
            float * c = reinterpret_cast<float*>(buffer);
            for(int i=0;i<num_elements;i++) {
                final_str.append(std::to_string(c[i])+"\n");
            }
            break;
        }
        case 7: {
            double * c = reinterpret_cast<double*>(buffer);
            for(int i=0;i<num_elements;i++) {
                final_str.append(std::to_string(c[i])+"\n");
            }
            break;
        }
    }
    final_str.append(final_str);
    fs << final_str;
    fs.close();
    free(buffer);
    return SUCCESS_STATUS;
}
#define ERRCODE 2
#define ERR(e) {printf("Error: %s\n", nc_strerror(e)); exit(ERRCODE);}

bool handle_pnetcdf(InputArgs args) {
    std::string filename=args.dir+"netcdf_"+std::to_string(args.dataType.index_)+".nc";
    size_t num_elements=args.size_mb_*MB/args.dataType.size_;
    void* buffer = generate_data(args);

    int ncid,x_dimid,varid,retval;
    int dims[1];
    herr_t      status;

    /* Create a new file using default properties. */
    if(retval=nc_create(filename.c_str(), NC_CLOBBER, &ncid)){
        ERR(retval);
    }
    if(retval=nc_def_dim(ncid, "x", num_elements, &x_dimid)) ERR(retval);
    nc_type type;
    switch(args.dataType.index_){
        case 1:{
            type=NC_BYTE;
            break;
        }
        case 2:
        case 3:{
            type=NC_INT;
            break;
        }
        case 4:
        case 5:{
            type=NC_INT;
            break;
        }
        case 6:{
            type=NC_FLOAT;
            break;
        }
        case 7:{
            type=NC_DOUBLE;
            break;
        }
    }
    dims[0] = x_dimid;
    if(retval=nc_def_var(ncid, "data", type, 1, dims, &varid))
        ERR(retval);
    if(retval=nc_enddef(ncid)) ERR(retval);
    switch(args.dataType.index_){
        case 1:{
            signed char* c=reinterpret_cast<signed char*>(buffer);
            int s=sizeof(signed char);

            if(retval= nc_put_var_schar(ncid, varid, c))
                ERR(retval);
            break;
        }
        case 2:
        case 3:{
            int* c=reinterpret_cast<int*>(buffer);
            if(retval=nc_put_var_int(ncid, varid,c)) ERR(retval);
            break;
        }
        case 4:
        case 5:{
            uint * c=reinterpret_cast<uint*>(buffer);
            if(retval=nc_put_var_uint(ncid,varid, c)) ERR(retval);
            break;
        }
        case 6:{
            float * c=reinterpret_cast<float*>(buffer);
            if(retval=nc_put_var_float(ncid,varid, c)) ERR(retval);
            break;
        }
        case 7:{
            double * c=reinterpret_cast<double*>(buffer);
            if(retval=nc_put_var_double(ncid,varid, c)) ERR(retval);
            break;
        }
    }
    if(retval=nc_close(ncid)) ERR(retval);
    free(buffer);
    return SUCCESS_STATUS;
}

bool handle_mpiio(InputArgs args) {
    std::string filename=args.dir+"mpiio_"+std::to_string(args.dataType.index_)+".dat";
    size_t num_elements=args.size_mb_*MB/args.dataType.size_;
    void* buffer = generate_data(args);
    MPI_File fh;
    MPI_Status status;
    MPI_Datatype type;
    switch(args.dataType.index_){
        case 1:{
            type=MPI_CHAR;
            break;
        }
        case 2:
        case 3:{
            type=MPI_INT;
            break;
        }
        case 4:
        case 5:{
            type=MPI_UINT32_T;
            break;
        }
        case 6:{
            type=MPI_FLOAT;
            break;
        }
        case 7:{
            type=MPI_DOUBLE;
            break;
        }
    }

    MPI_File_open(MPI_COMM_SELF, filename.c_str(), MPI_MODE_CREATE | MPI_MODE_RDWR, MPI_INFO_NULL, &fh);
    MPI_File_set_view(fh, 0, type, type, "native", MPI_INFO_NULL);
    MPI_File_write(fh, buffer, num_elements, type, &status);
    MPI_File_close(&fh);

    free(buffer);
    return SUCCESS_STATUS;
}

bool handle_hdf5(InputArgs args) {
    std::string filename=args.dir+"hdf5_"+std::to_string(args.dataType.index_)+".h5";
    size_t num_elements=args.size_mb_*MB/args.dataType.size_;
    void* buffer = generate_data(args);

    hid_t file_id, dataset_id, dataspace_id;
    hsize_t dims[1];
    herr_t      status;

    /* Create a new file using default properties. */
    file_id = H5Fcreate(filename.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    /* Create the data space for the dataset. */
    dims[0] = num_elements;
    dataspace_id = H5Screate_simple(1, dims, NULL);
    int64_t type;
    switch(args.dataType.index_){
        case 1:{
            type=H5T_NATIVE_CHAR;
            break;
        }
        case 2:
        case 3:{
            type=H5T_NATIVE_INT32;
            break;
        }
        case 4:
        case 5:{
            type=H5T_NATIVE_UINT32;
            break;
        }
        case 6:{
            type=H5T_NATIVE_FLOAT;
            break;
        }
        case 7:{
            type=H5T_NATIVE_DOUBLE;
            break;
        }
    }
    /* Create the dataset. */
    dataset_id = H5Dcreate2(file_id, "/dset", type, dataspace_id,
                            H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    /* Write the dataset. */
    status = H5Dwrite(dataset_id, type, H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer);
    /* End access to the dataset and release resources used by it. */
    status = H5Dclose(dataset_id);

    /* Terminate access to the data space. */
    status = H5Sclose(dataspace_id);

    /* Close the file. */
    status = H5Fclose(file_id);

    free(buffer);
    return SUCCESS_STATUS;
}

bool handle_binary_files(InputArgs args) {
    bool status=SUCCESS_STATUS;
    void* buffer = generate_data(args);
    std::string filename=args.dir+"binary_"+std::to_string(args.dataType.index_)+".bin";
    std::FILE *fh = std::fopen(filename.c_str(),"w+");
    size_t count=std::fwrite(buffer,args.dataType.size_,args.size_mb_*MB/args.dataType.size_,fh);
    if(count<args.size_mb_*MB){
        status= FAILURE;
    }
    free(buffer);
    fclose(fh);
    return status;
}

void * generate_data(InputArgs args) {
    void* data=malloc(args.size_mb_*MB);
    size_t num_elements=args.size_mb_*MB/args.dataType.size_;
    size_t offset=0;
    srand(200);

    switch (args.dataType.index_) {
        case 1: {
            for(int i=0;i<num_elements;++i) {
                int random = rand();
                char c = static_cast<char>((random % 26) + 'a');
                memcpy(data + offset, &c, sizeof(char));
                offset += sizeof(char);
            }
            break;
        }
        case 2: {
            std::vector<int> nums(num_elements);
            std::generate(nums.begin(),nums.end(),[](){
                int32_t random = rand();
                int32_t sign = rand() % 2;
                int32_t val = random;
                if (sign == 1) {
                    val = -1 * random;
                }
                return val;
            });
            std::sort(nums.begin(),nums.end());
            memcpy(data, nums.data(), sizeof(int32_t)*num_elements);
            break;
        }
        case 3: {
            for(int i=0;i<num_elements;++i) {
                int32_t random = rand();
                int32_t sign = rand() % 2;
                int32_t val = random;
                if (sign == 1) {
                    val = -1 * random;
                }
                memcpy(data + offset, &val, sizeof(int32_t));
                offset += sizeof(int32_t);
            }
            break;
        }
        case 4:{
            std::vector<uint32_t > nums(num_elements);
            std::generate(nums.begin(),nums.end(),[](){
                uint random = static_cast<uint32_t>(rand());
                return random;
            });
            std::sort(nums.begin(),nums.end());
            memcpy(data, nums.data(), sizeof(uint32_t)*num_elements);
            break;
        }
        case 5: {
            for(int i=0;i<num_elements;++i) {
                int random = rand();
                uint32_t val = static_cast<uint32_t>(random);
                memcpy(data + offset, &val, sizeof(uint32_t));
                offset += sizeof(uint32_t);
            }
            break;
        }
        case 6: {
            for(int i=0;i<num_elements;++i) {
                int random = rand();
                float val = static_cast<float>(static_cast<float>(random) / 100.0);
                memcpy(data + offset, &val, sizeof(float));
                offset += sizeof(float);
            }
            break;
        }
        case 7: {
            for(int i=0;i<num_elements;++i) {
                int random = rand();
                double val = static_cast<double>(random / 1000.0);
                memcpy(data + offset, &val, sizeof(double));
                offset += sizeof(double);
            }
            break;
        }
    }
    return data;
}

/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 9/19/18.
//

#ifndef ARES_UTILS_H
#define ARES_UTILS_H

#include <getopt.h>
#include <iostream>
#include "data_structure.h"
#include "error_codes.h"
static void display_usage(std::string executable)
{
    std::cout.flush();
    std::cout << "\nUsage:\n "<<executable<<"  [Options]\n" << std::endl;
    std::cout << "\nOptions:\n";
    std::cout << "-d <path>\t\tdirectory to put generated files\n";
    std::cout << "-s <size>\t\tSize of data in (MB)\n";
    std::cout << "-F <data-format>\t\tFormat of Data\n";
    std::cout << "-D <data-type>\t\t Data Type\n";
    //std::cout << "\nUsage:\n ./Binary  File_name(string) [-t data_type_chosen] [-l Library_to_use] [-m Compression_metric]\n" << std::endl;
    //std::cout << "(-m) Compression-metric options:\n 1 - Balanced (Default) \n 2 - Compression Ratio\n 3 - Compression Speed\n 4 - Decompression Speed" << std::endl;

}

static bool parse_opts(int argc, char *argv[], InputArgs &args){
    int flags, opt;
    int nsecs, tfnd;

    nsecs = 0;
    tfnd = 0;
    flags = 0;
    while ((opt = getopt (argc, argv, "d:F:D:s:")) != -1)
    {
        switch (opt)
        {
            case 'd':{
                args.dir=std::string(optarg);
                break;
            }
            case 's':{
                args.size_mb_= static_cast<size_t>(atoi(optarg));
                break;
            }
            case 'F':{
                args.format = DataFormat(atoi(optarg));;
                break;
            }
            case 'D':{
                args.dataType = DataType (static_cast<size_t>(atoi(optarg)));;
                break;
            }
            default:               /* '?' */
                display_usage(argv[0]);
                exit (EXIT_FAILURE);
        }
    }
    return SUCCESS_STATUS;
}



#endif //ARES_UTILS_H

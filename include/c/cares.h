/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 10/11/18.
//

#ifndef ARES_CARES_H
#define ARES_CARES_H

//------------------ Compression Routines -------------------

#include <stdint-gcc.h>

/* buffer to buffer - 2 default parameters. library choice = Only ares */
bool ares_compress(void * source, long unsigned int source_size, void * destination, long unsigned int destination_size, uint16_t data_type,
                    uint16_t library,uint16_t format,uint16_t priority);

/* buffer to file - 3 default parameters */
bool ares_compress(void * source, long unsigned int source_size, char*, uint16_t data_type, uint16_t library,uint16_t format,uint16_t priority);

/* file/dir to file/dir - 3 default parameters */
bool ares_compress( char*, uint16_t data_type, uint16_t library,uint16_t format,uint16_t priority);



//----------------- Decompression Routines ------------------

/* buffer to buffer */
bool ares_decompress(void *source, long unsigned int source_size, void *destination, long unsigned int destination_size, uint16_t data_type, uint16_t library,uint16_t format,uint16_t priority);

/* file to buffer */
bool ares_decompress(char*,void *destination, long unsigned int destination_size, uint16_t data_type, uint16_t library,uint16_t format,uint16_t priority);

/* file/dir to file/dir */
bool ares_decompress(char*, uint16_t data_type,uint16_t library,uint16_t format,uint16_t priority);

#endif //ARES_CARES_H

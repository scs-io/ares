/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 *
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

public class Ares {
  static {
    System.loadLibrary("ares");
  }

  public byte[] Compress(
      byte[] source, int sourceSize, short data_type, short library, short format, short priority) {

    byte[] destination = c_compress(source, sourceSize, data_type, library, format, priority);
    return destination;
  }

  public byte[] Decompress(
      byte[] source, int sourceSize, short data_type, short library, short format, short priority) {
    byte[] destination = c_decompress(source, sourceSize, data_type, library, format, priority);
    return destination;
  }

  private static native byte[] c_compress(
      byte[] source, int sourceSize, short data_type, short library, short format, short priority);

  private static native byte[] c_decompress(
      byte[] source, int sourceSize, short data_type, short library, short format, short priority);
}

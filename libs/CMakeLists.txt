cmake_minimum_required(VERSION 3.5)
project(external-libs)
set(CMAKE_CXX_STANDARD 11)
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Winline -O2 -no-pie -w -fPIC -D_FILE_OFFSET_BITS=64")
set(BCL_SRC bcl-1.2.0/src/rle.h bcl-1.2.0/src/rle.c
        bcl-1.2.0/src/shannonfano.h bcl-1.2.0/src/shannonfano.c
        bcl-1.2.0/src/rice.h bcl-1.2.0/src/rice.c
        bcl-1.2.0/src/lz.h bcl-1.2.0/src/lz.c)
add_library (bcl SHARED ${BCL_SRC})

set(BZIP2_SRC bzip2-1.0.6/blocksort.c
        bzip2-1.0.6/huffman.c
        bzip2-1.0.6/crctable.c bzip2-1.0.6/randtable.c
        bzip2-1.0.6/compress.c bzip2-1.0.6/decompress.c
        bzip2-1.0.6/bzlib.h bzip2-1.0.6/bzlib.c)
add_library (bz2 SHARED ${BZIP2_SRC})

set(HUFF_SRC huffman/huffman.h huffman/huffcode.c huffman/huffman.c)
add_library(huff SHARED ${HUFF_SRC})

add_subdirectory(lzo-2.10)

set(PITHY_SRC pithy-master/pithy.h pithy-master/pithy.c)
add_library(pithy SHARED ${PITHY_SRC})

set(QUICKLZ_SRC quicklz150beta2/quicklz.h quicklz150beta2/quicklz.c)
add_library(quicklz SHARED ${QUICKLZ_SRC})

add_subdirectory(snappy-master)

set(TRLE_SRC TurboRLE-master/trle.h TurboRLE-master/trlec.c  TurboRLE-master/trled.c TurboRLE-master/trle.c )
add_library(trle SHARED ${TRLE_SRC})

add_subdirectory(zlib-1.2.11)



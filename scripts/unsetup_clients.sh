#!/bin/bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
# Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of Ares
# 
# Ares is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
INSTALL_DIR=/home/cc/nfs/install
SCRIPTS_DIR=/home/cc/nfs/program/ares/scripts
umount -l --force /mnt/pfs 
ps -aef | grep pvfs | awk '{print $2}' | xargs kill -9
killall pvfs2-client
rmmod ${INSTALL_DIR}/lib/modules/`uname -r`/kernel/fs/pvfs2/pvfs2.ko 
rm -rf /mnt/pfs
mount | grep pvfs2

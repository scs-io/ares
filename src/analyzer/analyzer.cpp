/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 3/16/18.
//


#include <sys/types.h>
#include <sys/stat.h>
#include <cstdlib>
#include <cstring>
#include <cmath>

#include <vector>
#include <algorithm>
#include <stdexcept>
#include <typeinfo>
#include <iostream>
#include <exception>

#include "analyzer.h"
#include "../common/debug.h"
#include "../common/error_codes.h"
#include "../common/utilities.h"


bool Analyzer::Analyze(AnalyzerInput input, AnalyzerOutput &output) {

    if(input.format_!= DataFormat::DUMMY){
        output.format_=input.format_;
    }else{
        if(!ExtractFileFormat(input.filename_, output.format_)){
            return FAILURE;
        }
    }
    if(!input.compressionUnits.empty()){
        output.compressionUnits=input.compressionUnits;
    }else{
        if(!DeduceDataType(input.buffer_, input.buffer_size_, input.compressionUnits)){
            return FAILURE;
        }
    }
    return SUCCESS;
}


//------ FIGURE OUT THE INPUT's DATA-TYPE ON THE FLY -----
bool Analyzer::DeduceDataType(void *buffer, size_t buffer_size, DataTypeVector &data_types)
{
    //Check for null buffer
    if(!buffer){
        DEBUG("Analyzer::DeduceDataType(): Null buffer provided as input!");
        return FAILURE;
    }


    DataType data_type_basic,data_type;

    //Try to deduce the basic data type and map with the supported ones
    if(!ParseBasicType(buffer, data_type_basic)){
        return FAILURE;
    }

    if(data_type_basic == DataType::SINT_UNSORTED){
        if(!FindIntType(buffer, buffer_size, data_type)){
            //We found that there exists non-integers in the other portions of the data. So, just going with CHAR
            data_type = DataType::CHAR;
            //return FAILURE;
        }
    }
    else if(data_type_basic == DataType::FLOAT){
        data_type = DataType::FLOAT;
    }
    else{//The last choice
        data_type = DataType::CHAR;
    }
    data_types.push_back(CompressionUnit(0,buffer_size,data_type,CompressionLibrary::DUMMY));
    return SUCCESS;
}


//------ PARSE THE INPUT's (BASIC) DATA TYPE -----
bool Analyzer::ParseBasicType(void *buffer, DataType &data_type)
{
    //Check for null buffer
    if(!buffer){
        DEBUG("Analyzer::ParseBasicType(): Null buffer provided as input!");
        return FAILURE;
    }

    //Lets create a string object for testing
    char test_str16b[16];

    //Copy 16 bytes from the input buffer into the test string
    std::memcpy(test_str16b, buffer, sizeof(test_str16b));

    //Check for a decimal point in the string
    bool decimal_pt_flag = (std::strchr(test_str16b, '.') != nullptr);

    //Try to convert the test string into a float type
    float f_val = strtof(test_str16b, nullptr);

    //Check if the float conversion is successful
    if(f_val != 0){ //There exists a numeric value (float or int)!
        f_val = f_val - (long)f_val;

        //Check if there exists a fractional part
        if(f_val != 0 || decimal_pt_flag){ //The value could be 10.000000, who knows! So, || with decimal_pt_flag.
            data_type = DataType::FLOAT;
        }
        else{ //Number with no fractional part or decimal point, then must be an INT
            data_type = DataType::SINT_UNSORTED;
        }
    }
    else{ //If we can't find a numeric data-type we assume the input as characters
        data_type = DataType::CHAR;
    }

    return SUCCESS;
}


//------ FIND IN WHAT FORMAT THE INTEGER DATA-TYPE EXISTS --------
bool Analyzer::FindIntType(void *buffer, size_t &buffer_size, DataType &int_data_type)
{
    bool sint_flag = true;
    long sint_val = 0;
    std::vector<long> sint_vec;

    char* char_buffer = nullptr;
    char* char_buffer_start_ptr = nullptr;
    char* char_buffer_rest = nullptr;

    //Note: Precision in finding the right file-type might be increased by increasing the "num_elements" value
    unsigned int num_elements = 10;

    //Assuming an int value could hold upto 15 characters (including trailing ',', '\n', ' ') till the next value
    size_t read_size = 15 * num_elements;

    //Check for null buffer
    if(!buffer){
        DEBUG("Analyzer::FindIntType(): Null buffer provided as input!");
        return FAILURE;
    }

    //Check if we can read the values in the buffer and identify the right file type
    if(read_size > buffer_size){
        //Go with the buffer size
        read_size = buffer_size;

        //Check if some values can be read to determine the correct INT type
        num_elements = (unsigned int)read_size/15;
        if(num_elements < 1){
            DEBUG("Analyzer::FindIntType(): Number of INT elements to check is less than 1!");
            return FAILURE;
        }
    }

    //Create a character buffer of "read_size" bytes
    try{
        char_buffer = new char[read_size];
    }
    catch(std::exception &err){
        DEBUG("Analyzer::FindIntType(): char_buffer allocation (size:" << read_size <<" bytes) failed! Exception Msg: " << err.what());
        return FAILURE;
    }

    //Copy from the input buffer (of read_size) to the test (char) bufferd
    std::memcpy(char_buffer, buffer, read_size);

    //Store the location of char_buffer for releasing the allocated memory when needed
    char_buffer_start_ptr = char_buffer;

    //Convert the strings into long values for up to num_elements
    for(int i = 0; i < num_elements; ++i){

        //Convert the input string to a long value
        //We use std::strtol instead of std::stoi for overall efficiency
        sint_val = std::strtol(char_buffer, &char_buffer_rest, 10);

        if(0 == sint_val){ //One of the num_elements can't be converted to an integer. Could be a CHAR
            //Hereafter on the entire file data will be considered as of CHAR type
            delete [] char_buffer_start_ptr;
            return FAILURE;
        }

        //Check if any of the converted values crosses the signed INT's max boundary
        //It could cross, as we use signed long conversion instead of signed int
        if(sint_val > std::numeric_limits<int>::max()){
            sint_flag = false;
        }

        //Store the resulting values in a vector
        sint_vec.push_back(sint_val);

        //Check for comma separated input values. If exists, go past it
        try{
            if(char_buffer_rest[0] == ','){
                char_buffer_rest++;
            }
        }
        catch(std::exception &err){
            DEBUG("Analyzer::FindIntType(): Check for comma separated values failed! Exception Msg: " << err.what());
            delete [] char_buffer_start_ptr;
            return FAILURE;
        }


        //Move to the remaining string to process it in the next iteration
        char_buffer = char_buffer_rest;
        char_buffer_rest = nullptr;
    }

    //Check if the input strings are sorted
    if(std::is_sorted(sint_vec.begin(), sint_vec.end())){
        if(sint_flag){
            int_data_type = DataType::SINT_SORTED;
        }
        else{ //Either of the num_elements is not in the signed integer range
            int_data_type = DataType::UINT_SORTED;
        }
    }
    else{
        int_data_type = DataType::SINT_UNSORTED;
    }

    //Cleanup
    delete [] char_buffer_start_ptr;

    return SUCCESS;
}


//---- FIND IF THE GIVEN PATH IS A FILE OR A DIRECTORY ----
bool Analyzer::ExtractFileFormat(const std::string &file_path, DataFormat &format)
{
    std::string extension;
    GetFileExtension(file_path, extension);
    format=MapExtensionToFormat(extension);
    return !(format == DataFormat::DUMMY);
}




//********************************** EOF ******************************************

/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 3/16/18.
//

#ifndef ARES_TYPE_PARSER_H
#define ARES_TYPE_PARSER_H

#include <string>
#include "../common/enumerations.h"
#include "../common/data_structure.h"
#include "../common/error_codes.h"

//---CLASS: ANALYZER---
class Analyzer{
private:
    typedef std::vector<CompressionUnit> DataTypeVector;
    /**
     * private methods
     */
    bool ExtractFileFormat(const std::string &file_path,DataFormat &format);
    bool DeduceDataType(void *buffer, size_t buffer_size, DataTypeVector &data_types);
    bool ParseBasicType(void *buffer, DataType &data_type);
    bool FindIntType(void *buffer, size_t &buffer_size, DataType &data_type);


public:
    /**
     * constructors
     */
    Analyzer() = default; //Default Ctor
    ~Analyzer() = default; //Dtor
    Analyzer(const Analyzer&) = delete; //Copy Ctor
    Analyzer& operator=(const Analyzer&) = delete; // Assignment Optr
    Analyzer(Analyzer &&) = delete; // Move Ctor
    Analyzer& operator=(Analyzer&&) = delete; // Move Assignment Optr

    /**
     * public methods
     */
    bool Analyze(AnalyzerInput input, AnalyzerOutput &output);
};


#endif //ARES_TYPE_PARSER_H

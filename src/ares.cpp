/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 4/8/18.
//
#include <iostream>
#include <fstream>
#include <memory>
#include <vector>
#include <algorithm>
#include <exception>

#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cstdarg>
#include <dirent.h>
#include <sys/stat.h>

#include "../include/c++/ares.h"
#include "file_clients/io_manager.h"
#include "analyzer/analyzer.h"
#include "engine/engine.h"
#include "common/enumerations.h"
#include "common/debug.h"
#include "common/error_codes.h"
#include "common/singleton.h"
#include "compression_clients/lib_factory.h"
#include "formatter/formatter.h"
#include "common/utilities.h"


//------------- COMPRESSION - BUFFER to BUFFER -------------
bool ares_compress(const void * source, size_t source_size, void *&destination, size_t &destination_size, AresFlags &flags)
{

    //Check for NULL or empty source buffer
    if(source == nullptr || source_size == 0){
        DEBUG("ares_compress(buffer)->buffer: Source buffer is NULL, or it is empty (0 bytes)!");
        return FAILURE;
    }

    //Instantiate the helper classes
    Formatter &formatter = Singleton<Formatter>::GetInstance();
    Analyzer &analyzer = Singleton<Analyzer>::GetInstance();

    //Hard coding to ARES logic
    //As buffers don't have extensions like files, we need their meta data for decompression later
    //So, for buffer compressionUnits/decompression, only ARES logic exists
    auto library = flags.library;
    if(library==CompressionLibrary::ARES){
        //Get the chosen data-type

        //Deduce the data-type, if it isn't given
        AnalyzerOutput analyzerOutput;
        if(flags.compressionUnits.empty()) {
            AnalyzerInput analyzerInput;
            analyzerInput.format_=DataFormat::DUMMY;
            analyzerInput.buffer_=source;
            analyzerInput.buffer_size_=source_size;
            if (!analyzer.Analyze(analyzerInput,analyzerOutput)) {
                DEBUG("ares_compress(buffer)->buffer: The data type of the buffer can't be determined!");
                return FAILURE;
            }
        }else{
            analyzerOutput.format_=flags.format_;
            analyzerOutput.compressionUnits=std::vector<CompressionUnit>();
            analyzerOutput.compressionUnits.insert(analyzerOutput.compressionUnits.begin(),flags.compressionUnits.begin(),flags.compressionUnits.end());
        }

        //Find the chosen performance metric before instantiating the engine with it

        //Instantiate the Compress/Decompress engine with the correct compressionUnits metric
        Engine &engine = Singleton<Engine>::GetInstance();
        EngineInput engineInput(analyzerOutput, flags.priority);
        EngineOutput engineOutput;
        engine.predict(engineInput, engineOutput);
        flags.compressionUnits=std::vector<CompressionUnit>();
        flags.compressionUnits.insert(flags.compressionUnits.begin(),engineOutput.compressionUnits.begin(),engineOutput.compressionUnits.end());
    }else{
        flags.compressionUnits=std::vector<CompressionUnit>();
        flags.compressionUnits.emplace_back(CompressionUnit(0,source_size,DataType::DUMMY,library));
    }
     DEBUG("compressionUnits :"<<flags.compressionUnits.size());
    //Perform Compression
    //Instantiate library factory
    LibFactory &lib_factory = LibFactory::GetInstance();
    size_t totalSize=0;
    std::vector<std::pair<void*,size_t>> destinations=std::vector<std::pair<void*,size_t>>();
    for(auto compressionUnit:flags.compressionUnits){
        //Get the library based on its chosen index
        auto comp_lib = lib_factory.get_library(compressionUnit.library);

        //If the compression library exits, use it to compress the source buffer
        if(comp_lib){
            //Compress
            void * destination_temp = nullptr;
            void* source_temp=source+compressionUnit.start_;
            size_t destination_temp_size=0;
            if(comp_lib->compress(source_temp,source_size, destination_temp, destination_temp_size)){
                std::cout << "Compression " << comp_lib->time_elapsed_msec() <<","<< (double)source_size/destination_temp_size<< std::endl;
                destinations.emplace_back(std::make_pair(destination_temp,destination_temp_size));
                totalSize+=destination_temp_size;
            }
            else{
                DEBUG("Engine::compress(): Compression Failed!");
                return FAILURE;
            }
        }
        else{
            DEBUG("Engine::compress(): Error! No library exists for the given index");
            return FAILURE;
        }
    }
    void* finalDestination=malloc(totalSize);
    for(auto destinationPair:destinations){
        memcpy(finalDestination,destinationPair.first,destinationPair.second);
        free(destinationPair.first);
    }
    destination=finalDestination;
    destination_size=totalSize;
    //Fill the meta data info
    Metadata metadata(flags.compressionUnits, source_size, destination_size,flags.format_);
    metadata.compressedSize += metadata.GetSize();
    destination_size=metadata.compressedSize;
    //Concatenate meta data as a prefix to the analyzerOutput buffer
    if(!formatter.encode(destination, destination_size,metadata)){
        DEBUG("DECODE FAILED");
        return FAILURE;
    }
    DEBUG("COMPRESSION SUCCESS");
    return SUCCESS;
}


//------------- DECOMPRESSION - BUFFER to BUFFER -------------
bool ares_decompress(const void *source, size_t source_size, void *&destination, size_t &destination_size, AresFlags &flags)
{
    //Check for NULL source buffer
    if(source == nullptr){
        DEBUG("ares_decompress(buffer)->buffer: Source buffer is NULL!");
        return FAILURE;
    }

    //Instantiate the helper class
    Formatter &formatter = Singleton<Formatter>::GetInstance();

    //Init a meta data structure
    Metadata metadata;

    //Read the meta data info
    formatter.decode(source,source_size,metadata);
    //Let the caller know of the output (uncompressed) buffer size
    destination_size = metadata.uncompressedSize;
    flags.format_=metadata.format_;
    flags.compressionUnits=metadata.compressionUnits;

    //Perform Decompression using the meta data available
    //Note: Skip the meta-data part and send only the compressed data part for decompression

    LibFactory &lib_factory = LibFactory::GetInstance();
    destination=malloc(destination_size);
    void* compressed_source=malloc(metadata.compressedSize*sizeof(char));
    memcpy(compressed_source,source+metadata.GetSize(),metadata.compressedSize);
    //free(source);
    source=compressed_source;
    size_t offset=0;
    for(auto compressionUnit:metadata.compressionUnits){
        auto comp_lib = lib_factory.get_library(compressionUnit.library);

        //If the compression library exits, use it to decompress the source buffer
        if(comp_lib){
            void *destination_temp;
            void* source_temp=source+compressionUnit.start_;
            size_t destination_size_temp=destination_size;
            //Decompress
            size_t uncompressed_size;
            if(comp_lib->decompress(source_temp, metadata.compressedSize-metadata.GetSize(), destination_temp, destination_size_temp)){

                std::cout << "Decompression " << comp_lib->time_elapsed_msec()<< std::endl;
                memcpy(destination+offset,destination_temp,destination_size_temp);
                offset+=destination_size_temp;
                free(destination_temp);
            }
            else{
                DEBUG("Engine::decompress(): Decompression Failed!");
                return FAILURE;
            }
        }
        else{
            DEBUG("Engine::decompress(): Error! No library exists for the given index!");
            return FAILURE;
        }
    }
    return SUCCESS;
}


//------------- COMPRESSION - BUFFER to FILE -------------
bool ares_compress(void *&source, size_t source_size, std::string output_file, AresFlags &flags)
{
    void *destination = nullptr;
    size_t destination_size = 0;

    //Check for NULL or empty source buffer
    if(source == nullptr || source_size == 0){
        DEBUG("ares_compress(buffer)->file: Source buffer is NULL, or it is empty (0 bytes)!");
        return FAILURE;
    }
    if(!ares_compress(source,source_size,destination,destination_size,flags)){
        DEBUG("ares_compress(): Compression Failed!");
        return FAILURE;
    }
    //Instantiate the helper classes
    IOManager &io_mgr = Singleton<IOManager>::GetInstance();
    if(!io_mgr.write(output_file,destination,destination_size,DataFormat::ARES,true)){
        DEBUG("ares_compress(): Compression Failed!");
        return FAILURE;
    }
    free(destination);
    destination = nullptr;
    return SUCCESS;
}


//------------- DECOMPRESSION - FILE to BUFFER -------------
bool ares_decompress(std::string file_path, void *&destination, size_t &destination_size,
                     AresFlags &flags)
{
    void *source;
    size_t source_size = 0;
    IOManager &io_mgr = Singleton<IOManager>::GetInstance();
    io_mgr.read(file_path,source,source_size);
    return ares_decompress(source,source_size,destination,destination_size,flags);
}

//------------- COMPRESSION - FILE/DIR to FILE/DIR -------------
bool ares_compress(std::string file_path, AresFlags &flags)
{
    std::cout<<"filepath:"<<file_path<<"\n";
    IOManager &io_mgr = Singleton<IOManager>::GetInstance();
    if(IsFile(file_path)){
        void *source= nullptr;
        size_t source_size = 0;
        io_mgr.read(file_path,source,source_size);
        ares_compress(source, source_size, file_path,flags);
        free(source);
        //remove(file_path.c_str());
    }else{
        DIR *dir;
        struct dirent *dir_entry;
        //Open the directory
        if( (dir = opendir(file_path.c_str())) != nullptr ) {
            //Read directory contents
            while((dir_entry = readdir(dir)) != nullptr){
                //Note: Sub-directories in the given directory aren't going to be processed(now). So, skip them.
                if (dir_entry->d_type != DT_DIR){ //if(dir_entry->d_name != "." || dir_entry->d_name != ". ."){
                    void *source;
                    size_t source_size = 0;
                    io_mgr.read(dir_entry->d_name,source,source_size);
                    ares_compress(source, source_size, dir_entry->d_name,flags);
                    free(source);
                    //remove(dir_entry->d_name);
                }
            }
            //Cleanup
            closedir (dir);
        }
    }
    return SUCCESS;
}

//------------- DECOMPRESSION - FILE/DIR to FILE/DIR -------------
bool ares_decompress(std::string file_path,AresFlags &flags)
{

    IOManager &io_mgr = Singleton<IOManager>::GetInstance();
    if(IsFile(file_path)){
        void *source;
        size_t source_size = 0;
        ares_decompress(file_path,source, source_size,flags);
        //io_mgr.write(file_path,source,source_size,flags.format_,false);
        free(source);
        //remove(file_path.c_str());
    }else{
        DIR *dir;
        struct dirent *dir_entry;
        //Open the directory
        if( (dir = opendir(file_path.c_str())) != nullptr ) {
            //Read directory contents
            while((dir_entry = readdir(dir)) != nullptr){
                //Note: Sub-directories in the given directory aren't going to be processed(now). So, skip them.
                if (dir_entry->d_type != DT_DIR){ //if(dir_entry->d_name != "." || dir_entry->d_name != ". ."){
                    void *source;
                    size_t source_size = 0;
                    ares_decompress(dir_entry->d_name,source, source_size,flags);
                    io_mgr.write(dir_entry->d_name,source,source_size,flags.format_,false);
                    free(source);
                    //remove(dir_entry->d_name);
                }
            }
            //Cleanup
            closedir (dir);
        }
    }
    return SUCCESS;
}


//****************************** EOF *********************************//

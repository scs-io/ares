/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 10/11/18.
//

#include "../include/c/cares.h"
#include "common/data_structure.h"
#include "../include/c++/ares.h"

bool ares_compress(void * &source, long unsigned int source_size, void *&destination, long unsigned int &destination_size, uint16_t data_type,uint16_t library,
              uint16_t format, uint16_t priority) {
    std::vector<CompressionUnit> compressionUnits=std::vector<CompressionUnit>();
    compressionUnits.emplace_back(CompressionUnit(0,source_size,DataType(data_type),CompressionLibrary (library)));
    AresFlags flags=AresFlags(CompressionLibrary (library),DataFormat (format),compressionUnits,PriorityType (priority));

    return ares_compress(source,source_size,destination,destination_size,flags);
}

bool ares_compress(void *&source, long unsigned int source_size, char *, uint16_t data_type, uint16_t library, uint16_t format, uint16_t priority) {
    return false;
}

bool ares_compress(char *, uint16_t library, uint16_t format, uint16_t priority) {
    return false;
}

bool
ares_decompress(void *&source, long unsigned int source_size, void *&destination, uint32_t &destination_size, uint16_t data_type, uint16_t library,
                uint16_t format, uint16_t priority) {
    return false;
}

bool ares_decompress(char *, void *&destination, long unsigned int &destination_size, uint16_t data_type, uint16_t library, uint16_t format,
                     uint16_t priority) {
    return false;
}

bool ares_decompress(char *, uint16_t data_type, uint16_t library, uint16_t format, uint16_t priority) {
    return false;
}


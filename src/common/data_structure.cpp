/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 9/19/18.
//
#include "data_structure.h"

DataType* DataType::DUMMY = new DataType(0,0,false);
DataType* DataType::CHAR = new DataType(1,sizeof(char),false);
DataType* DataType::SINT_SORTED= new DataType(2,sizeof(int),true);
DataType* DataType::SINT_UNSORTED= new DataType(3,sizeof(int),false);
DataType* DataType::UINT_SORTED= new DataType(4,sizeof(uint),true);
DataType* DataType::UINT_UNSORTED= new DataType(5,sizeof(uint),false);
DataType* DataType::FLOAT= new DataType(6,sizeof(float),false);
DataType* DataType::DOUBLE= new DataType(7,sizeof(double),false);

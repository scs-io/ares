/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 9/17/18.
//

#ifndef ARES_DATA_STRUCTURE_H
#define ARES_DATA_STRUCTURE_H

#include <string>
#include <vector>
#include "enumerations.h"
#include <cstring>
#include <iostream>

typedef struct DataType //"source_type" argument in ares_compress()
{
    size_t index_;
    size_t size_;
    bool is_sorted_;
    static DataType *DUMMY,*CHAR,*SINT_SORTED,*SINT_UNSORTED,
                    *UINT_SORTED,*UINT_UNSORTED,*FLOAT,*DOUBLE;
    DataType(size_t index_,size_t size,bool sorted):index_(index_),size_(size),is_sorted_(sorted){}
    DataType(const DataType &other):index_(other.index_),is_sorted_(other.is_sorted_),size_(other.size_){}
    DataType(const DataType *other):index_(other->index_),is_sorted_(other->is_sorted_),size_(other->size_){}
    DataType(size_t index){
        this->index_=index;
        switch(index){
            case 0:{this->size_=DUMMY->size_;this->is_sorted_=DUMMY->is_sorted_; break;}
            case 1:{this->size_=CHAR->size_;this->is_sorted_=CHAR->is_sorted_;break;}
            case 2:{this->size_=SINT_SORTED->size_;this->is_sorted_=SINT_SORTED->is_sorted_;break;}
            case 3:{this->size_=SINT_UNSORTED->size_;this->is_sorted_=SINT_UNSORTED->is_sorted_;break;}
            case 4:{this->size_=UINT_SORTED->size_;this->is_sorted_=UINT_SORTED->is_sorted_;break;}
            case 5:{this->size_=UINT_UNSORTED->size_;this->is_sorted_=UINT_UNSORTED->is_sorted_;break;}
            case 6:{this->size_=FLOAT->size_;this->is_sorted_=FLOAT->is_sorted_;break;}
            case 7:{this->size_=DOUBLE->size_;this->is_sorted_=DOUBLE->is_sorted_;break;}
        }
    }
    DataType(){
        this->size_=DUMMY->size_;
        this->is_sorted_=DUMMY->is_sorted_;
    }
    ~DataType(){}
    bool operator==(const DataType other){
        return this->index_==other.index_;
    }
    bool operator==(const DataType* other){
        return this->index_==other->index_;
    }
} DataType;


typedef struct CompressionUnit{
    DataType dataType;
    CompressionLibrary library;

    int start_;
    int end_;
    CompressionUnit():start_(0),end_(0),dataType(dataType.DUMMY),library(CompressionLibrary::DUMMY){}
    CompressionUnit(int start_, int end_,DataType dataType,CompressionLibrary library)
            :start_(start_),end_(end_),dataType(dataType),library(library){}
    static size_t GetSize(){
        return sizeof(DataType)+sizeof(CompressionLibrary)+2*sizeof(int);
    }
    size_t GetDimentionSize(){
        return end_-start_;
    }
} CompressionUnit;


typedef struct AnalyzerInput{
    /**
     * for analysis
     */
    std::string filename_;
    void* buffer_;
    size_t buffer_size_;
    /**
     * user_defined
     */
    DataFormat format_;
    std::vector<CompressionUnit> compressionUnits;
    AnalyzerInput():filename_(),buffer_(nullptr),buffer_size_(0),format_(DataFormat::DUMMY),compressionUnits(){}
} AnalyzerInput;


typedef struct AnalyzerOutput{
    DataFormat format_;
    std::vector<CompressionUnit> compressionUnits;
    AnalyzerOutput():format_(),compressionUnits(){}
}AnalyzerOutput;


typedef struct EngineInput{
    AnalyzerOutput parameters;
    PriorityType priorityType;
    EngineInput():parameters(),priorityType(PriorityType::W_1_1_1){}
    EngineInput(AnalyzerOutput parameter, PriorityType priorityType):parameters(parameter),priorityType(priorityType){}

} EngineInput;

typedef struct EngineOutput{
    std::vector<CompressionUnit> compressionUnits;
    EngineOutput():compressionUnits(){}
} EngineOutput;

typedef struct AresFlags{
    CompressionLibrary library;
    DataFormat format_;
    std::vector<CompressionUnit> compressionUnits;
    PriorityType priority;
    AresFlags():format_(DataFormat::BINARY),compressionUnits(),priority(PriorityType::W_1_1_1){}
    AresFlags(CompressionLibrary library,DataFormat format_,std::vector<CompressionUnit> compressionUnits,PriorityType priority):
            library(library),format_(format_),compressionUnits(compressionUnits),priority(priority){}
} AresFlags;

//Meta-data Info
struct Metadata{
    std::vector<CompressionUnit> compressionUnits;
    int uncompressedSize;
    int compressedSize;
    DataFormat format_;
    size_t GetSize(){
        return 3*sizeof(int)+compressionUnits.size()*CompressionUnit::GetSize()+ sizeof(DataFormat);
    }
    Metadata():compressionUnits(),compressedSize(0),uncompressedSize(0),format_(DataFormat::BINARY){}
    Metadata(std::vector<CompressionUnit> compressionUnits, size_t uncompressedSize, size_t compressedSize,DataFormat format_)
            :compressionUnits(compressionUnits),compressedSize(compressedSize),
             uncompressedSize(uncompressedSize),format_(format_){}
    void* encode(){
        void* buf=malloc(GetSize());
        int vectorSize=compressionUnits.size();
        memcpy(buf,&vectorSize,sizeof(int));
        
        size_t offset=sizeof(int);
        for(auto compressionUnit:compressionUnits){
            memcpy(buf+offset,&compressionUnit.dataType,sizeof(DataType));
            offset+=sizeof(DataType);
            memcpy(buf+offset,&compressionUnit.library,sizeof(CompressionLibrary));
            offset+=sizeof(CompressionLibrary);
            memcpy(buf+offset,&compressionUnit.start_,sizeof(int));
            offset+=sizeof(int);
            memcpy(buf+offset,&compressionUnit.end_,sizeof(int));
            offset+=sizeof(int);
        }
        std:: memcpy(buf+offset,&compressedSize,sizeof(int));
        offset+=sizeof(int);
        memcpy(buf+offset,&uncompressedSize,sizeof(int));
        offset+=sizeof(int);
        memcpy(buf+offset,&format_,sizeof(DataFormat));
        offset+=sizeof(DataFormat);
        return buf;
    }
    void decode(const void* buf){
        int vectorSize;
        size_t offset=0;
        memcpy(&vectorSize,buf+offset,sizeof(int));
        offset+=sizeof(int);
        compressionUnits.clear();
        for(int i=0;i<vectorSize;++i){
            CompressionUnit compressionUnit;
            DataType dataType;
            memcpy(&dataType,buf+offset,sizeof(DataType));
            offset+=sizeof(DataType);
            CompressionLibrary library;
            memcpy(&library,buf+offset,sizeof(CompressionLibrary));
            offset+=sizeof(CompressionLibrary);
            int start_,end_;
            memcpy(&start_,buf+offset,sizeof(int));
            offset+=sizeof(int);
            memcpy(&end_,buf+offset,sizeof(int));
            offset+=sizeof(int);
            compressionUnits.emplace_back(CompressionUnit(start_,end_,dataType,library));
        }
        memcpy(&compressedSize,buf+offset,sizeof(int));
        offset+=sizeof(int);
        memcpy(&uncompressedSize,buf+offset,sizeof(int));
        offset+=sizeof(int);
        memcpy(&format_,buf+offset,sizeof(DataFormat));
        offset+=sizeof(int);
    }
};



#endif //ARES_DATA_STRUCTURE_H

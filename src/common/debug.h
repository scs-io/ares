/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 9/17/18.
//

#ifndef ARES_DEBUG_H
#define ARES_DEBUG_H
//---------------| DEBUG MESSAGE SWITCH |---------------

//#define DEBUG_BUILD //Comment this line to turn off debug messages

#ifdef DEBUG_BUILD
#define DEBUG(x) do { std::cerr << x << std::endl; } while (0)
#else
#define DEBUG(x) do {} while (0)
#endif
#endif //ARES_DEBUG_H

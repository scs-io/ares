/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 9/17/18.
//

#ifndef ARES_SINGLETON_H
#define ARES_SINGLETON_H
#include <iostream>
#include <memory>
/**
 * Make a class singleton when used with the class. format for class name T
 * Singleton<T>::GetInstance()
 * @tparam T
 */
template<typename T>
class Singleton {
public:
    /**
     * Members of Singleton Class
     */
    static T& GetInstance(); /*static method to get instance of class T. */

    /**
     * Operators
     */
    Singleton& operator= (const Singleton) = delete; /* deleting = operatos*/
    /**
     * Constructor
     */
public:
    Singleton(const Singleton&) = delete; /* deleting copy constructor. */
protected:
    Singleton() {} /* hidden default constructor. */
};
/**
 * Uses unique pointer to build a static global instance of variable.
 * @tparam T
 * @return instance of T
 */
template<typename T>
T& Singleton<T>::GetInstance()
{
    static const std::unique_ptr<T> instance{new T{}}; /* const unique pointer ensures global instance. */
    return *instance;
}
#endif //ARES_SINGLETON_H

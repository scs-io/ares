/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 9/18/18.
//

#ifndef ARES_UTILITIES_H
#define ARES_UTILITIES_H

#include <string>
#include "error_codes.h"
#include "enumerations.h"
#include "debug.h"
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>

static bool GetFileExtension(const std::string &s, std::string &ext) {
    size_t i = s.rfind('.', s.length());
    if (i != std::string::npos) {
        ext=(s.substr(i+1, s.length() - i));
        return SUCCESS;
    }
    return FAILURE;
}

//---- FIND IF THE GIVEN PATH IS A FILE OR A DIRECTORY ----
static bool IsFile(const std::string &file_path){
    struct stat stat_buf = {0};
    return stat(file_path.c_str(), &stat_buf) == 0 && stat_buf.st_mode & S_IFREG;
}

//-----------MAP LIBRARY INDEX TO THE FILE's EXTENSION--------------
static std::string MapFormatToExtension(DataFormat format)
{
    std::string extension;

    switch(format){
        case DataFormat::ARES:
            extension = "ares";
            break;

        case DataFormat::BINARY:
            extension = "bin";
            break;

        case DataFormat::MPIIO:
            extension = "mpiio";
            break;

        case DataFormat::HDF5:
            extension = "h5";
            break;

        case DataFormat::NETCDF:
            extension = "h5";
            break;

        case DataFormat::CSV:
            extension = "csv";
            break;

        case DataFormat::JSON:
            extension = "json";
            break;
        case DataFormat::XML:
            extension = "xml";
            break;


        case DataFormat::PARQUET:
            extension = "parque";
            break;

        case DataFormat::AVRO:
            extension = "avro";
            break;
        default: //CompressionLibrary::DUMMY
            extension = "dummy";
            DEBUG("File extension doesn't exist for the given lib_index!");
    }

    return extension;
}


static DataFormat MapExtensionToFormat(std::string extension)
{
    if(extension == "ares"){
        return DataFormat::ARES;
    }else if(extension == "bin"){
        return DataFormat::BINARY;
    }else if(extension == "h5"){
        return DataFormat::HDF5;
    }else if(extension == "h5"){
        return DataFormat::HDF5;
    }else if(extension == "h5"){
        return DataFormat::HDF5;
    }else if(extension == "h5"){
        return DataFormat::HDF5;
    }else if(extension == "nc"){
        return DataFormat::NETCDF;
    }else if(extension == "csv"){
        return DataFormat::CSV;
    }else if(extension == "json"){
        return DataFormat::JSON;
    }else if(extension == "xml"){
        return DataFormat::XML;
    }else if(extension == "parquet"){
        return DataFormat::PARQUET;
    }else if(extension == "avro"){
        return DataFormat::AVRO;
    }else{
        return DataFormat::DUMMY;
    }
}

template <typename Enumeration>
auto as_integer(Enumeration const value)
-> typename std::underlying_type<Enumeration>::type
{
    return static_cast<typename std::underlying_type<Enumeration>::type>(value);
}

#endif //ARES_UTILITIES_H

/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/14/17.
//

#include <iostream>

#include "bz2_client.h"
#include "../common/debug.h"
#include "../common/error_codes.h"

extern "C" {
#include "bzlib.h"
}

using std::cerr;
using std::endl;

//-----------BZ2 COMPRESSION------------
bool BZ2client::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    int bz2error = 0;

    //Estimate destination buffer size
    destination_size = source_size+128*1024*1024;

    //Allocate destination buffer
    destination = (char*)malloc(destination_size * sizeof(char));
    if(nullptr == destination){
        DEBUG("Error in BZ2 Compression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    timer.start();

    //Compress
    bz2error = BZ2_bzBuffToBuffCompress((char*)destination, (unsigned int*)&destination_size, (char*)source, (unsigned int)source_size, 9, 0 ,0);
    if(BZ_OK != bz2error){
        DEBUG("Error in BZ2 Compression, @ BZ2_bzBuffToBuffCompress()!"+std::to_string(bz2error));
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//-----------BZ2 DECOMPRESSION------------
bool BZ2client::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    int bz2error = 0;
    unsigned int COMP_RATIO_GUESS = 5;

    //Estimate destination buffer size if nothing is supplied
    if(0 == destination_size){
        destination_size = source_size * COMP_RATIO_GUESS;
    }


    //Allocate destination buffer
    destination = (char*)malloc(destination_size * sizeof(char));
    if(nullptr == destination){
        DEBUG("Error in BZ2 Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    timer.start();

    do{
        //Decompress
        bz2error = BZ2_bzBuffToBuffDecompress((char*)destination, (unsigned int*)&destination_size, (char*)source, (unsigned int)source_size, 1 ,0);

        if(BZ_OK != bz2error){

            if(BZ_OUTBUFF_FULL == bz2error){ //Destination/Uncompressed buffer size is small!
                //Re-estimate output buffer size
                destination_size = (source_size * COMP_RATIO_GUESS * 2) * sizeof(char); //Double the size of previous estimation. Will also increase the execution time!

                //Reallocate output buffer
                destination = (char*)realloc(destination, destination_size);
                if(nullptr == destination){
                    DEBUG("Error in BZ2 Decompression, @ realloc()!");
                    free(destination);
                    destination = nullptr;
                    return FAILURE;
                }
            }
            else{
                DEBUG("Error in BZ2 Decompression, @ BZ2_bzBuffToBuffDecompress()!");
                free(destination);
                destination = nullptr;
                return FAILURE;
            }
        }
    }while(BZ_OUTBUFF_FULL == bz2error);


    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//****************************** EOF *********************************//

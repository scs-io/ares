/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/14/17.
//

#include <cstring>
#include <iostream>

#include "huff_client.h"
#include "../common/debug.h"
#include "../common/error_codes.h"

extern "C"{
#include "huffman.h"
}


using std::cerr;
using std::endl;

//-----------HUFFMAN COMPRESSION------------
bool HUFFclient::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //Estimate destination buffer size
    destination_size = source_size;

    //Allocate destination buffer
    destination = (char*)malloc(destination_size * sizeof(char));
    if(nullptr == destination){
        DEBUG("Error in Huffman Compression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    timer.start();

    //Compress
    //Note: Send the destination buffer portion post the stored original size info
    if(huffman_encode_memory((const unsigned char*)source, (unsigned int)source_size, (unsigned char**)destination, (unsigned int*)&destination_size)){
        DEBUG("Error in Huffman Compression, @ huffman_encode_memory()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//-----------HUFFMAN DECOMPRESSION------------
bool HUFFclient::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    int huffError = 1;
    //*destination = &huffError;

    if(0 == destination_size) {
        //Get the original/uncompressed size from the info stored during compression
        //std::memcpy(&destination_size, source, sizeof(size_t));
        destination_size = source_size * 2;
    }

    //Allocate destination buffer
    destination = (char*)malloc(destination_size*sizeof(char));
    if(nullptr == destination){
        DEBUG("Error in Huffman Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    timer.start();

    //Decompress
    //Note: Send the source/compressed buffer portion post the stored original size info
    huffError = huffman_decode_memory((const unsigned char*)source+sizeof(destination_size), (unsigned int)source_size, (unsigned char**)destination, (unsigned int*)&destination_size);
    if(huffError==1)
    {
        DEBUG("Error in Huffman Decompression, @ huffman_decode_memory()!"+huffError);
        free(destination);
        destination = nullptr;
        return FAILURE;
    }



    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//****************************** EOF *********************************//

/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/13/17.
//

#ifndef ARES_LIBCLIENT_H
#define ARES_LIBCLIENT_H

#include "../common/time_counter.h"
typedef void*& SOURCE_TYPE;
typedef void*& DESTINATION_TYPE;

class LibClient
{
public:
    LibClient() = default; //Default Ctor
    ~LibClient() = default; //Default Dtor

    //Get execution time in milliseconds
    double time_elapsed_msec()
    {
        return timer.get_duration_msec();
    }

    //virtual void init() = 0;
    virtual bool compress(SOURCE_TYPE, size_t, DESTINATION_TYPE, size_t&) = 0;
    virtual bool decompress(SOURCE_TYPE, size_t, DESTINATION_TYPE, size_t&) = 0;

protected:
    TimeCounter timer;
};

#endif //ARES_LIBCLIENT_H

/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/13/17.
//

#include <memory>
#include "lib_factory.h"


//------GET LIBRARY FACTORY (SINGLETON) INSTANCE-----
LibFactory& LibFactory::GetInstance()
{
    static LibFactory s_instance;
    return s_instance;
}

//------GET LIBRARY METHOD------
std::unique_ptr<LibClient> LibFactory::get_library(CompressionLibrary lib_index)
{
    switch(lib_index){
        case CompressionLibrary::BZ2:
            return std::unique_ptr<BZ2client>(new BZ2client);
        case CompressionLibrary::ZLIB:
            return std::unique_ptr<ZLIBclient>(new ZLIBclient);
        case CompressionLibrary::HUFF:
            return std::unique_ptr<HUFFclient>(new HUFFclient);
        case CompressionLibrary::SF:
            return std::unique_ptr<SFclient>(new SFclient);
        case CompressionLibrary::RICE:
            return std::unique_ptr<RICEclient>(new RICEclient);
        case CompressionLibrary::RLE:
            return std::unique_ptr<RLEclient>(new RLEclient);
        case CompressionLibrary::TRLE:
            return std::unique_ptr<TRLEclient>(new TRLEclient);
        case CompressionLibrary::LZO:
            return std::unique_ptr<LZOclient>(new LZOclient);
        case CompressionLibrary::PITHY:
            return std::unique_ptr<PITHYclient>(new PITHYclient);
        case CompressionLibrary::SNAPPY:
            return std::unique_ptr<SNAPPYclient>(new SNAPPYclient);
        case CompressionLibrary::QLZ:
            return std::unique_ptr<QLZclient>(new QLZclient);
        default: //If the library doesn't exist
            return nullptr;
    }
}

//****************************** EOF *********************************//

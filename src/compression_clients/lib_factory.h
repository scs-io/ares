/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/13/17.
//

#ifndef ARES_LIBFACTORY_H
#define ARES_LIBFACTORY_H


#include <map>
#include <mutex>
#include <memory>

#include "lib_client.h"
#include "bz2_client.h"
#include "zlib_client.h"
#include "huff_client.h"
#include "sf_client.h"
#include "rice_client.h"
#include "rle_client.h"
#include "trle_client.h"
#include "lzo_client.h"
#include "pithy_client.h"
#include "snappy_client.h"
#include "qlz_client.h"
#include "../common/enumerations.h"

using std::map;

class LibFactory
{
public:
    static LibFactory& GetInstance(); //Get Singleton instance

    ~LibFactory() = default; //Default Dtor

    LibFactory(const LibFactory&) = delete; //No Copy Ctor
    LibFactory& operator=(const LibFactory&) = delete; //No Assignment Optr
    LibFactory(LibFactory &&) = delete; // No Move Ctor
    LibFactory&operator=(LibFactory&&) = delete; //No Move Assignment Optr

    std::unique_ptr<LibClient> get_library(CompressionLibrary);

private:
    LibFactory() = default; //Default Ctor
};

#endif //ARES_LIBFACTORY_H

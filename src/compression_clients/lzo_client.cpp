/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/14/17.
//

#include <iostream>

#include "lzo_client.h"
#include "../common/debug.h"
#include "../common/error_codes.h"

extern "C" {
#include "lzo/lzo1x.h"
#include "lzo/lzoconf.h"
#include "lzo/lzodefs.h"
}

using std::cerr;
using std::endl;

//-----------LZO COMPRESSION------------
bool LZOclient::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    lzo_voidp work_mem = nullptr;

    //LZO init
    if(lzo_init() != LZO_E_OK){
        DEBUG("Error in LZO Compression, @ lzo_init()!\n");
        return FAILURE;
    }

    //Allocate work memory
    work_mem = malloc(LZO1X_1_MEM_COMPRESS);

    //Estimate destination buffer size
    destination_size = source_size + source_size / 16 + 64 + 3; //as recommended in doc

    //Allocate destination buffer
    destination = (lzo_bytep)malloc(destination_size * sizeof(lzo_byte));
    if(nullptr == destination){
        DEBUG("Error in LZO Compression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    timer.start();

    //Compress
    if (LZO_E_OK != lzo1x_1_compress((const lzo_bytep)source, (lzo_uint)source_size, (lzo_bytep)destination, &destination_size, work_mem)){
        DEBUG("Error in LZO Compression, @ lzo1x_1_compress()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    free(work_mem);

    return SUCCESS;
}

//-----------LZO DECOMPRESSION------------
bool LZOclient::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    int lzoError = 0;
    unsigned int COMP_RATIO_GUESS = 3;

    //LZO init
    if(lzo_init() != LZO_E_OK){
        DEBUG("Error in LZO Decompression, @ lzo_init()!\n");
        return FAILURE;
    }

    //Estimate destination buffer size
    if(0 == destination_size) {
        destination_size = source_size * COMP_RATIO_GUESS;
    }

    //Allocate destination buffer
    destination = (lzo_bytep)malloc(destination_size * sizeof(lzo_byte));
    if(nullptr == destination){
        DEBUG("Error in LZO Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    timer.start();

    do{
        //Decompress
        lzoError = lzo1x_decompress((const lzo_bytep)source, (lzo_uint)source_size, (lzo_bytep)destination, &destination_size, NULL);

        if(LZO_E_OK != lzoError){

            if(LZO_E_OUTPUT_OVERRUN == lzoError){ //Destination/Uncompressed buffer size is small!
                //Re-estimate output buffer size
                destination_size = (source_size * COMP_RATIO_GUESS * 2) * sizeof(char); //Double the size of previous estimation

                //Reallocate output buffer
                destination = (char*)realloc(destination, destination_size);
                if(nullptr == destination){
                    DEBUG("\"Error in LZO Decompression, @ realloc()!");
                    free(destination);
                    destination = nullptr;
                    return FAILURE;
                }
            }
            else{
                DEBUG("Error in LZO Decompression, @ lzo1x_1_decompress()!");
                free(destination);
                destination = nullptr;
                return FAILURE;
            }
        }
    }while(LZO_E_OUTPUT_OVERRUN == lzoError);

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//****************************** EOF *********************************//

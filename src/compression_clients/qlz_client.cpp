/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/14/17.
//

#include <iostream>

#include "qlz_client.h"
#include "../common/debug.h"
#include "../common/error_codes.h"

extern "C" {
#include "quicklz.h"
}

using std::cerr;
using std::endl;

//-----------QLZ COMPRESSION------------
bool QLZclient::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    size_t comp_size = 0;

    //Init compression state
    auto *state_compress = (qlz_state_compress*)malloc(sizeof(qlz_state_compress));
    memset(state_compress, 0, sizeof(qlz_state_compress));

    //Estimate destination buffer size
    destination_size = source_size;

    //Allocate destination buffer
    destination = (char*)malloc((destination_size + 400) * sizeof(char)); //as recommended in doc
    if(nullptr == destination){
        DEBUG("Error in QLZ Compression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    timer.start();

    //Compress
    comp_size = qlz_compress(source, (char*)destination, source_size, state_compress);
    if (!comp_size){
        DEBUG("Error in QLZ Compression, @ QLZ_compress()!");
        free(state_compress);
        free(destination);
        destination = nullptr;
        return FAILURE;
    }
    destination_size = comp_size;

    //---Time measure ends---
    timer.stop();

    free(state_compress);

    return SUCCESS;
}

//-----------QLZ DECOMPRESSION------------
bool QLZclient::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    size_t uncomp_size = 0;

    //Init decompression state
    auto *state_decompress = (qlz_state_decompress *)malloc(sizeof(qlz_state_decompress));
    memset(state_decompress, 0, sizeof(qlz_state_decompress));

    //Estimate destination buffer size
    //destination_size = qlz_size_decompressed((const char*)source);

    //Allocate destination buffer
    destination = (char*)malloc((destination_size + 400) * sizeof(char));//as recommended in doc
    if(nullptr == destination){
        DEBUG("Error in QLZ Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    timer.start();

    //Decompress
    uncomp_size = qlz_decompress((const char*)source, destination, state_decompress);
    if (!uncomp_size){
        DEBUG("Error in QLZ Decompression, @ QLZ_compress()!");
        free(state_decompress);
        free(destination);
        destination = nullptr;
        return FAILURE;
    }
    destination_size = uncomp_size;

    //---Time measure ends---
    timer.stop();

    free(state_decompress);

    return SUCCESS;
}

//****************************** EOF *********************************//

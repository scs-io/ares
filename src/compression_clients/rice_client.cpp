/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/14/17.
//

#include <iostream>

#include "rice_client.h"
#include "../common/debug.h"
#include "../common/error_codes.h"

extern "C" {
#include "rice.h"
}

using std::cerr;
using std::endl;

//-----------RICE COMPRESSION------------
bool RICEclient::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //Estimate destination buffer size
    destination_size = source_size + 1;

    //Allocate destination buffer
    destination = (char*)malloc(destination_size * sizeof(char));
    if(nullptr == destination){
        DEBUG("Error in Rice Compression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    timer.start();

    //Compress
    destination_size = (size_t)Rice_Compress((unsigned char*)source, (unsigned char*)destination, (unsigned int)source_size, RICE_FMT_INT8);
    if(!destination_size){
        DEBUG("Error in RICE Compression, @ Rice_Compress()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//-----------RICE DECOMPRESSION------------
bool RICEclient::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //Estimate destination buffer size
    if(0 == destination_size) {
        destination_size = source_size * 4;
    }

    //Allocate destination buffer
    destination = (char*)malloc(destination_size * sizeof(char));
    if(nullptr == destination){
        DEBUG("Error in Rice Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    timer.start();

    //Decompress
    Rice_Uncompress((unsigned char*)source, (unsigned char*)destination, (unsigned int)source_size, (unsigned int)destination_size, RICE_FMT_INT8);
    if(!destination_size){
        DEBUG("Error in RICE Decompression, @ Rice_Uncompress()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//****************************** EOF *********************************//

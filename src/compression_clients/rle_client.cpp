/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/14/17.
//

#include <cstring>
#include <iostream>

#include "rle_client.h"
#include "../common/debug.h"
#include "../common/error_codes.h"

extern "C" {
#include "rle.h"
}

using std::cerr;
using std::endl;

//-----------RLE COMPRESSION------------
bool RLEclient::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //Estimate destination buffer size
    destination_size = source_size * (257/256 + 1);

    //Allocate destination buffer
    destination = (char*)malloc(destination_size * sizeof(char));
    if(nullptr == destination){
        DEBUG("Error in RLE Compression, @ malloc()!");
        return FAILURE;
    }

    //Store the uncompressed buffer size at the start of the compressed buffer - like meta data
    std::memcpy(destination, &source_size, sizeof(size_t));

    //---Time measure begins---
    timer.start();

    //Compress
    //Note: Send the destination buffer portion post the stored original size info
    destination_size = (size_t)RLE_Compress((unsigned char*)source, (unsigned char*)destination + sizeof(size_t), (unsigned int)source_size);
    if(!destination_size){
        DEBUG("Error in RLE Compression, @ RLE_Compress()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//-----------RLE DECOMPRESSION------------
bool RLEclient::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    if(0 == destination_size) {
        //Get the original/uncompressed size from the info stored during compression
        std::memcpy(&destination_size, source, sizeof(size_t));
    }

    //Allocate destination buffer
    destination = (char*)malloc(destination_size * sizeof(char));
    if(nullptr == destination){
        DEBUG("Error in RLE Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    timer.start();

    //Decompress
    //Note: Send the source/compressed buffer portion post the stored original size info
    RLE_Uncompress((unsigned char*)source + sizeof(size_t), (unsigned char*)destination, (unsigned int)source_size);
    if(!destination_size){
        DEBUG("Error in RLE Decompression, @ RLE_Uncompress()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//****************************** EOF *********************************//

/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/14/17.
//

#include <iostream>

#include <cstring>

#include "sf_client.h"
#include "../common/debug.h"
#include "../common/error_codes.h"

extern "C" {
#include "shannonfano.c"
}

using std::cerr;
using std::endl;

//-----------SHANNON-FANO COMPRESSION------------
bool SFclient::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //Estimate destination buffer size
    destination_size = source_size+1024*4; //(as suggested, in bcl lib's manual.doc

    //Allocate destination buffer
    destination = (unsigned char*)malloc(destination_size * sizeof(unsigned char));
    if(nullptr == destination){
        DEBUG("Error in Shannon-Fano Compression, @ malloc()!");
        return FAILURE;
    }
    memcpy(destination,source,source_size);
    //---Time measure begins---
    timer.start();

    //Compress
    //Note: Send the destination buffer portion post the stored original size info
    destination_size = (size_t)SF_Compress(static_cast<unsigned char *>(source),
                                           static_cast<unsigned char *>(destination), source_size);
    if(!destination_size){
        DEBUG("Error in Shannon-Fano Compression, @ SF_Compress()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//-----------SHANNON-FANO DECOMPRESSION------------
bool SFclient::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //Allocate destination bufferd
    destination = (unsigned char*)malloc(destination_size * sizeof(unsigned char));
    if(nullptr == destination){
        DEBUG("Error in Shannon-Fano Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    timer.start();

    //Decompress
    //Note: Send the source/compressed buffer portion post the stored original size info
    SF_Uncompress((unsigned char*)source + sizeof(size_t), (unsigned char*)destination, (unsigned int)source_size- sizeof(size_t), (unsigned int)destination_size);
    if(!destination_size){
        DEBUG("Error in Shannon-Fano Decompression, @ SF_Uncompress()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//****************************** EOF *********************************//

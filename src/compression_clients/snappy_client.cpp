/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/14/17.
//

#include <iostream>

#include "snappy_client.h"
#include "../common/debug.h"
#include "../common/error_codes.h"

extern "C"{
#include "snappy-c.h"
}

using std::cerr;
using std::endl;

//-----------SNAPPY COMPRESSION------------
bool SNAPPYclient::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //Estimate destination buffer size
    destination_size = snappy_max_compressed_length(source_size);

    //Allocate destination buffer
    destination = (char*)malloc(destination_size * sizeof(char));
    if(nullptr == destination){
        DEBUG("Error in SNAPPY Compression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    timer.start();

    //Compress
    auto status=snappy_compress((const char*)source, source_size, (char*)destination, &destination_size);
    if (status != SNAPPY_OK){
        DEBUG("Error in SNAPPY Compression, @ snappy_compress()!"+std::to_string(status));
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//-----------SNAPPY DECOMPRESSION------------
bool SNAPPYclient::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    int snappyError = 0;

    //Estimate destination buffer size
    /*if (snappy_uncompressed_length((const char*)source, source_size, &destination_size)!= SNAPPY_OK){
        DEBUG("Error in SNAPPY Decompression, @ snappy_uncompressed_length()!");
        return FAILURE;
    }*/
    //Allocate destination buffer
    destination = (char*)malloc(destination_size * sizeof(char));
    if(nullptr == destination){
        DEBUG("Error in SNAPPY Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    timer.start();

    //Decompress
    snappyError = snappy_uncompress((const char*)source, source_size, (char*)destination, &destination_size);
    /*if(snappyError != SNAPPY_OK){
        DEBUG("Error in SNAPPY Decompression, @ snappy_uncompress()!"+std::to_string(snappyError));
        free(destination);
        destination = nullptr;
        return FAILURE;
    }*/

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//****************************** EOF *********************************//

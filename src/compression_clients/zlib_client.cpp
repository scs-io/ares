/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/14/17.
//

#include <iostream>

#include "zlib_client.h"
#include "../common/debug.h"
#include "../common/error_codes.h"

extern "C" {
#include "zlib.h"
}

using std::cerr;
using std::endl;

//-----------ZLIB COMPRESSION------------
bool ZLIBclient::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //Setting Compression Level:
    int level = Z_BEST_COMPRESSION, ret = 0;

    //Estimate destination buffer size
    destination_size = compressBound(source_size)+2*1024*1024;

    //Allocate destination buffer
    destination = (char*)malloc(destination_size * sizeof(char));
    if(nullptr == destination){
        DEBUG("Error in ZLIB Compression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    timer.start();

    //Compress
    ret = compress2((Bytef*)destination, &destination_size, (const Bytef*)source, source_size, level);
    if(ret != Z_OK){
        DEBUG("Error in ZLIB Compression, @ compress2()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//-----------ZLIB DECOMPRESSION------------
bool ZLIBclient::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    int ret = 0;

    //Estimate destination buffer size
    if(0 == destination_size) {
        destination_size = source_size * 4;
    }

    //Allocate destination buffer
    destination = (char*)malloc(destination_size * sizeof(char));
    if(nullptr == destination){
        DEBUG("Error in ZLIB Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    timer.start();

    //Decompress
    ret = uncompress2((Bytef*)destination, &destination_size, (const Bytef*)source, &source_size);
    if(ret != Z_OK){
        DEBUG("Error in ZLIB Decompression, @ uncompress2()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//****************************** EOF *********************************//

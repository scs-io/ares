/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.hadoop.io.compress.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.hadoop.conf.Configurable;
import org.apache.hadoop.conf.Configuration;


/**
 * This class creates ares compressors/decompressors.
 */
public class AresCodec implements Configurable, CompressionCodec {

  /** Internal buffer size for Ares compressor/decompressors */
  public static final String IO_COMPRESSION_CODEC_ARES_BUFFERSIZE_KEY =
      "io.compression.codec.ares.buffersize";

      private int IO_COMPRESSION_CODEC_ARES_DATATYPE_DEFAULT=0;
          private int IO_COMPRESSION_CODEC_ARES_LIBRARY_DEFAULT=1;
          private int IO_COMPRESSION_CODEC_ARES_FORMAT_DEFAULT=0;
          private int IO_COMPRESSION_CODEC_ARES_PRIORITY_DEFAULT=0;



  /** Default value for IO_COMPRESSION_CODEC_ARES_BUFFERSIZE_KEY */
  public static final int IO_COMPRESSION_CODEC_ARES_BUFFERSIZE_DEFAULT =
      256 * 1024;
      public static final String IO_COMPRESSION_CODEC_ARES_DATATYPE_KEY =
                        "io.compression.codec.ares.datatype";
                public static final String IO_COMPRESSION_CODEC_ARES_LIBRARY_KEY =
                        "io.compression.codec.ares.library";
                public static final String IO_COMPRESSION_CODEC_ARES_FORMAT_KEY =
                        "io.compression.codec.ares.format";
                public static final String IO_COMPRESSION_CODEC_ARES_PRIORITY_KEY =
                        "io.compression.codec.ares.priority";

  static {
    LoadAres.isLoaded();
  }

  Configuration conf;

  /**
   * Set the configuration to be used by this object.
   *
   * @param conf the configuration object.
   */
  @Override
  public void setConf(Configuration conf) {
    this.conf = conf;
  }

  /**
   * Return the configuration used by this object.
   *
   * @return the configuration object used by this objec.
   */
  @Override
  public Configuration getConf() {
    return conf;
  }

  /**
   * Are the native ares libraries loaded & initialized?
   *
   * @param conf configuration
   * @return true if loaded & initialized, otherwise false
   */
  public static boolean isNativeAresLoaded(Configuration conf) {
    return LoadAres.isLoaded();
  }

  /**
   * Create a {@link CompressionOutputStream} that will write to the given
   * {@link OutputStream}.
   *
   * @param out the location for the final output stream
   * @return a stream the user can write uncompressed data to have it compressed
   * @throws IOException
   */
  @Override
  public CompressionOutputStream createOutputStream(OutputStream out)
      throws IOException {
    return createOutputStream(out, createCompressor());
  }

  /**
   * Create a {@link CompressionOutputStream} that will write to the given
   * {@link OutputStream} with the given {@link Compressor}.
   *
   * @param out        the location for the final output stream
   * @param compressor compressor to use
   * @return a stream the user can write uncompressed data to have it compressed
   * @throws IOException
   */
  @Override
  public CompressionOutputStream createOutputStream(OutputStream out,
                                                    Compressor compressor)
      throws IOException {
    if (!isNativeAresLoaded(conf)) {
      throw new RuntimeException("native ares library not available");
    }
    int bufferSize = conf.getInt(
        IO_COMPRESSION_CODEC_ARES_BUFFERSIZE_KEY,
        IO_COMPRESSION_CODEC_ARES_BUFFERSIZE_DEFAULT);

    int compressionOverhead = 0;

    return new BlockCompressorStream(out, compressor, bufferSize,
        compressionOverhead);
  }

  /**
   * Get the type of {@link Compressor} needed by this {@link CompressionCodec}.
   *
   * @return the type of compressor needed by this codec.
   */
  @Override
  public Class<? extends Compressor> getCompressorType() {
    if (!isNativeAresLoaded(conf)) {
      throw new RuntimeException("native ares library not available");
    }

    return AresCompressor.class;
  }

  /**
   * Create a new {@link Compressor} for use by this {@link CompressionCodec}.
   *
   * @return a new compressor for use by this codec
   */
  @Override
  public Compressor createCompressor() {
    if (!isNativeAresLoaded(conf)) {
      throw new RuntimeException("native ares library not available");
    }
    int bufferSize = conf.getInt(
        IO_COMPRESSION_CODEC_ARES_BUFFERSIZE_KEY,
        IO_COMPRESSION_CODEC_ARES_BUFFERSIZE_DEFAULT);
        short data_type = (short)conf.getInt(
                        IO_COMPRESSION_CODEC_ARES_DATATYPE_KEY,
                        IO_COMPRESSION_CODEC_ARES_DATATYPE_DEFAULT);
                short library = (short)conf.getInt(
                        IO_COMPRESSION_CODEC_ARES_LIBRARY_KEY,
                        IO_COMPRESSION_CODEC_ARES_LIBRARY_DEFAULT);
                short format = (short)conf.getInt(
                        IO_COMPRESSION_CODEC_ARES_FORMAT_KEY,
                        IO_COMPRESSION_CODEC_ARES_FORMAT_DEFAULT);
                short priority =(short) conf.getInt(
                        IO_COMPRESSION_CODEC_ARES_PRIORITY_KEY,
                        IO_COMPRESSION_CODEC_ARES_PRIORITY_DEFAULT);

    return new AresCompressor(bufferSize,data_type,library,format,priority);
  }

  /**
   * Create a {@link CompressionInputStream} that will read from the given
   * input stream.
   *
   * @param in the stream to read compressed bytes from
   * @return a stream to read uncompressed bytes from
   * @throws IOException
   */
  @Override
  public CompressionInputStream createInputStream(InputStream in)
      throws IOException {
    return createInputStream(in, createDecompressor());
  }

  /**
   * Create a {@link CompressionInputStream} that will read from the given
   * {@link InputStream} with the given {@link Decompressor}.
   *
   * @param in           the stream to read compressed bytes from
   * @param decompressor decompressor to use
   * @return a stream to read uncompressed bytes from
   * @throws IOException
   */
  @Override
  public CompressionInputStream createInputStream(InputStream in,
                                                  Decompressor decompressor)
      throws IOException {
    if (!isNativeAresLoaded(conf)) {
      throw new RuntimeException("native ares library not available");
    }

    return new BlockDecompressorStream(in, decompressor, conf.getInt(
        IO_COMPRESSION_CODEC_ARES_BUFFERSIZE_KEY,
        IO_COMPRESSION_CODEC_ARES_BUFFERSIZE_DEFAULT));
  }

  /**
   * Get the type of {@link Decompressor} needed by this {@link CompressionCodec}.
   *
   * @return the type of decompressor needed by this codec.
   */
  @Override
  public Class<? extends Decompressor> getDecompressorType() {
    if (!isNativeAresLoaded(conf)) {
      throw new RuntimeException("native ares library not available");
    }

    return AresDecompressor.class;
  }

  /**
   * Create a new {@link Decompressor} for use by this {@link CompressionCodec}.
   *
   * @return a new decompressor for use by this codec
   */
  @Override
  public Decompressor createDecompressor() {
    if (!isNativeAresLoaded(conf)) {
      throw new RuntimeException("native ares library not available");
    }
    int bufferSize = conf.getInt(
        IO_COMPRESSION_CODEC_ARES_BUFFERSIZE_KEY,
        IO_COMPRESSION_CODEC_ARES_BUFFERSIZE_DEFAULT);
        short data_type = (short)conf.getInt(
                        IO_COMPRESSION_CODEC_ARES_DATATYPE_KEY,
                        IO_COMPRESSION_CODEC_ARES_DATATYPE_DEFAULT);
                short library = (short)conf.getInt(
                        IO_COMPRESSION_CODEC_ARES_LIBRARY_KEY,
                        IO_COMPRESSION_CODEC_ARES_LIBRARY_DEFAULT);
                short format = (short)conf.getInt(
                        IO_COMPRESSION_CODEC_ARES_FORMAT_KEY,
                        IO_COMPRESSION_CODEC_ARES_FORMAT_DEFAULT);
                short priority =(short) conf.getInt(
                        IO_COMPRESSION_CODEC_ARES_PRIORITY_KEY,
                        IO_COMPRESSION_CODEC_ARES_PRIORITY_DEFAULT);


    return new AresDecompressor(bufferSize,data_type,library,format,priority);
  }

  /**
   * Get the default filename extension for this kind of compression.
   *
   * @return <code>.ares</code>.
   */
  @Override
  public String getDefaultExtension() {
    return ".ares";
  }
}
/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "AresCompressor.h"
#include "../../include/c++/ares.h"
#include <iostream>
#include <cstring>
/* A helper macro to 'throw' a java exception. */ 
#define THROW(env, exception_name, message) \
  { \
	jclass ecls = env->FindClass(exception_name); \
	if (ecls) { \
	  env->ThrowNew( ecls, message); \
	  env->DeleteLocalRef( ecls); \
	} \
  }
#define LOCK_CLASS(env, clazz, classname) \
  if (env->MonitorEnter(clazz) != 0) { \
    char exception_msg[128]; \
    snprintf(exception_msg, 128, "Failed to lock %s", classname); \
    THROW(env, "java/lang/InternalError", exception_msg); \
  }

#define UNLOCK_CLASS(env, clazz, classname) \
  if (env->MonitorExit(clazz) != 0) { \
    char exception_msg[128]; \
    snprintf(exception_msg, 128, "Failed to unlock %s", classname); \
    THROW(env, "java/lang/InternalError", exception_msg); \
  }

static jfieldID AresCompressor_clazz;
static jfieldID AresCompressor_uncompressedDirectBuf;
static jfieldID AresCompressor_uncompressedDirectBufLen;
static jfieldID AresCompressor_compressedDirectBuf;
static jfieldID AresCompressor_directBufferSize;


static jfieldID AresCompressor_data_type;
static jfieldID AresCompressor_library;
static jfieldID AresCompressor_format;
static jfieldID AresCompressor_priority;

JNIEXPORT void JNICALL Java_AresCompressor_initIDs
(JNIEnv *env, jclass clazz){

  

  AresCompressor_clazz = env->GetStaticFieldID( clazz, "clazz",
                                                 "Ljava/lang/Class;");
  AresCompressor_uncompressedDirectBuf = env->GetFieldID( clazz,
                                                           "uncompressedDirectBuf",
                                                           "Ljava/nio/ByteBuffer;");
  AresCompressor_uncompressedDirectBufLen = env->GetFieldID( clazz,
                                                              "uncompressedDirectBufLen", "I");
  AresCompressor_compressedDirectBuf = env->GetFieldID( clazz,
                                                         "compressedDirectBuf",
                                                         "Ljava/nio/ByteBuffer;");
  AresCompressor_directBufferSize = env->GetFieldID( clazz,
                                                       "directBufferSize", "I");
                                                       
  AresCompressor_data_type= env->GetFieldID( clazz,"data_type", "S");
  AresCompressor_library= env->GetFieldID( clazz,"library", "S");
  AresCompressor_format= env->GetFieldID( clazz,"format", "S");   
  AresCompressor_priority= env->GetFieldID( clazz,"priority", "S");    
  std::cout<<"loaded all variabled IDs\n";
  }

JNIEXPORT jint JNICALL Java_AresCompressor_compressBytesDirect
(JNIEnv *env, jobject thisj){
   // Get members of AresCompressor
  jobject clazz = env->GetStaticObjectField( thisj, AresCompressor_clazz);
  jobject uncompressed_direct_buf = env->GetObjectField(thisj, AresCompressor_uncompressedDirectBuf);
  jint uncompressed_direct_buf_len = env->GetIntField( thisj, AresCompressor_uncompressedDirectBufLen);
  jobject compressed_direct_buf = env->GetObjectField( thisj, AresCompressor_compressedDirectBuf);
  jint directBufferSize = env->GetIntField( thisj, AresCompressor_directBufferSize);
  jshort data_type=env->GetShortField( thisj, AresCompressor_data_type);
  jshort library=env->GetShortField( thisj, AresCompressor_library);
  jshort format=env->GetShortField( thisj, AresCompressor_format);
  jshort priority=env->GetShortField( thisj, AresCompressor_priority);
  std::cout<<"Initialized everything6 " <<" uncompressed_direct_buf_len:"<<uncompressed_direct_buf_len
                                        <<" data_type:"<<data_type
                                        <<" library:"<<library
                                        <<" format:"<<format
                                        <<" priority:"<<priority;
  // Get the input direct buffer
  LOCK_CLASS(env, clazz, "AresCompressor");
  void*  uncompressed_bytes = (void* )env->GetDirectBufferAddress(uncompressed_direct_buf);
  UNLOCK_CLASS(env, clazz, "AresCompressor");  
  if (uncompressed_bytes == 0) {
    return (jint)0;
  }
  std::vector<CompressionUnit> compressionUnits=std::vector<CompressionUnit>();
  compressionUnits.emplace_back(CompressionUnit(0,uncompressed_direct_buf_len,DataType(data_type),CompressionLibrary (library)));
  AresFlags flags=AresFlags(CompressionLibrary (library),DataFormat (format),compressionUnits,PriorityType (priority));
  size_t compressed_len=directBufferSize;
  void* destination_buf;
  ares_compress( uncompressed_bytes, uncompressed_direct_buf_len, destination_buf, compressed_len, flags);
  env->SetIntField(thisj, AresCompressor_uncompressedDirectBufLen, 0);
  compressed_direct_buf = env->NewDirectByteBuffer(destination_buf,compressed_len);
  env->SetObjectField(thisj, AresCompressor_compressedDirectBuf, compressed_direct_buf);
  //free(destination_buf);
  return (jint)compressed_len;
}

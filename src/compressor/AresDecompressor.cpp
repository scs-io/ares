/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "AresDecompressor.h"
#include "../../include/c++/ares.h"
#include <iostream>
#include <cstring>
/* A helper macro to 'throw' a java exception. */ 
#define THROW(env, exception_name, message) \
  { \
	jclass ecls = env->FindClass(exception_name); \
	if (ecls) { \
	  env->ThrowNew( ecls, message); \
	  env->DeleteLocalRef( ecls); \
	} \
  }
#define LOCK_CLASS(env, clazz, classname) \
  if (env->MonitorEnter(clazz) != 0) { \
    char exception_msg[128]; \
    snprintf(exception_msg, 128, "Failed to lock %s", classname); \
    THROW(env, "java/lang/InternalError", exception_msg); \
  }

#define UNLOCK_CLASS(env, clazz, classname) \
  if (env->MonitorExit(clazz) != 0) { \
    char exception_msg[128]; \
    snprintf(exception_msg, 128, "Failed to unlock %s", classname); \
    THROW(env, "java/lang/InternalError", exception_msg); \
  }
static jfieldID AresDecompressor_clazz;
static jfieldID AresDecompressor_uncompressedDirectBuf;
static jfieldID AresDecompressor_compressedDirectBufLen;
static jfieldID AresDecompressor_compressedDirectBuf;
static jfieldID AresDecompressor_directBufferSize;


static jfieldID AresDecompressor_data_type;
static jfieldID AresDecompressor_library;
static jfieldID AresDecompressor_format;
static jfieldID AresDecompressor_priority;

JNIEXPORT void JNICALL Java_AresDecompressor_initIDs
(JNIEnv *env, jclass clazz){  

  AresDecompressor_clazz = env->GetStaticFieldID( clazz, "clazz",
                                                 "Ljava/lang/Class;");
                                                 std::cout<<"Initialized everything21\n";
  AresDecompressor_uncompressedDirectBuf = env->GetFieldID( clazz,
                                                           "uncompressedDirectBuf",
                                                           "Ljava/nio/ByteBuffer;");
  AresDecompressor_compressedDirectBufLen = env->GetFieldID( clazz,
                                                              "compressedDirectBufLen", "I");
  AresDecompressor_compressedDirectBuf = env->GetFieldID( clazz,
                                                         "compressedDirectBuf",
                                                         "Ljava/nio/ByteBuffer;");
  AresDecompressor_directBufferSize = env->GetFieldID( clazz,
                                                       "directBufferSize", "I");
  AresDecompressor_data_type= env->GetFieldID( clazz,"data_type", "S");
  AresDecompressor_library= env->GetFieldID( clazz,"library", "S");
  AresDecompressor_format= env->GetFieldID( clazz,"format", "S");   
  AresDecompressor_priority= env->GetFieldID( clazz,"priority", "S");               
                                                                                                    
  }
JNIEXPORT jint JNICALL Java_AresDecompressor_decompressBytesDirect
(JNIEnv *env, jobject thisj){
  // Get members of AresDecompressor
  jobject clazz = env->GetStaticObjectField( thisj, AresDecompressor_clazz);
  jobject uncompressed_direct_buf = env->GetObjectField(thisj, AresDecompressor_uncompressedDirectBuf);
  jint compressed_direct_buf_len = env->GetIntField( thisj, AresDecompressor_compressedDirectBufLen);
  jobject compressed_direct_buf = env->GetObjectField( thisj, AresDecompressor_compressedDirectBuf);
  jint directBufferSize = env->GetIntField( thisj, AresDecompressor_directBufferSize);
  jshort data_type=env->GetShortField( thisj, AresDecompressor_data_type);
  jshort library=env->GetShortField( thisj, AresDecompressor_library);
  jshort format=env->GetShortField( thisj, AresDecompressor_format);
  jshort priority=env->GetShortField( thisj, AresDecompressor_priority);
  // Get the input direct buffer
  LOCK_CLASS(env, clazz, "AresDecompressor");
  void* compressed_bytes = (void*)env->GetDirectBufferAddress(compressed_direct_buf);
  UNLOCK_CLASS(env, clazz, "AresDecompressor");

  if (compressed_bytes == 0) {
    return (jint)0;
  }
  std::vector<CompressionUnit> compressionUnits=std::vector<CompressionUnit>();
  compressionUnits.emplace_back(CompressionUnit(0,compressed_direct_buf_len,DataType(data_type),CompressionLibrary (library)));
  AresFlags flags=AresFlags(CompressionLibrary (library),DataFormat (format),compressionUnits,PriorityType (priority));
  size_t uncompressed_len=directBufferSize;
  void* destination_buf;
  ares_decompress( compressed_bytes, compressed_direct_buf_len, destination_buf, uncompressed_len, flags);
  std::cout<<"decompression done:"<<uncompressed_len<<"\n";
  env->SetIntField(thisj, AresDecompressor_compressedDirectBufLen, 0);
  uncompressed_direct_buf = env->NewDirectByteBuffer(destination_buf,uncompressed_len);
  env->SetObjectField(thisj, AresDecompressor_uncompressedDirectBuf, uncompressed_direct_buf);
  std::cout<<"decompression ready to return\n";
  //free(destination_buf);
  return (jint)uncompressed_len;
}

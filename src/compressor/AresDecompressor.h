/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class AresDecompressor */

#ifndef _Included_AresDecompressor
#define _Included_AresDecompressor
#ifdef __cplusplus
extern "C" {
#endif
#undef AresDecompressor_DEFAULT_DIRECT_BUFFER_SIZE
#define AresDecompressor_DEFAULT_DIRECT_BUFFER_SIZE 65536L
#undef AresDecompressor_DEFAULT_DIRECT_DATATYPE_DEFAULT
#define AresDecompressor_DEFAULT_DIRECT_DATATYPE_DEFAULT 0L
#undef AresDecompressor_DEFAULT_DIRECT_LIBRARY_DEFAULT
#define AresDecompressor_DEFAULT_DIRECT_LIBRARY_DEFAULT 11L
#undef AresDecompressor_DEFAULT_DIRECT_FORMAT_DEFAULT
#define AresDecompressor_DEFAULT_DIRECT_FORMAT_DEFAULT 0L
#undef AresDecompressor_DEFAULT_DIRECT_PRIORITY_DEFAULT
#define AresDecompressor_DEFAULT_DIRECT_PRIORITY_DEFAULT 0L
/*
 * Class:     AresDecompressor
 * Method:    initIDs
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_AresDecompressor_initIDs
  (JNIEnv *, jclass);
/*
 * Class:     AresDecompressor
 * Method:    decompressBytesDirect
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_AresDecompressor_decompressBytesDirect
  (JNIEnv *, jobject);

#ifdef __cplusplus
}
#endif
#endif

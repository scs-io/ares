/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/13/17.
//
#include <iostream>
#include <memory>
#include <typeinfo>

#include "engine.h"
#include "../compression_clients/lib_client.h"
#include "../compression_clients/lib_factory.h"
#include "../common/enumerations.h"
#include "../common/debug.h"
#include "../common/error_codes.h"
#include "../common/utilities.h"


bool Engine::predict(EngineInput input,EngineOutput &output) {
    for(int i=0;i<input.parameters.compressionUnits.size();++i){
       input.parameters.compressionUnits[i].library=right_lib[as_integer(input.priorityType)][as_integer(input.parameters.format_)][input.parameters.compressionUnits[i].dataType.index_];
    }
    output.compressionUnits=input.parameters.compressionUnits;
    return SUCCESS;
}

Engine::Engine():right_lib() {
    DataFormatArray dataFormatMap=DataFormatArray();
    DataTypeArray dummyFormatDataTypeMap=DataTypeArray();
    DataTypeArray binaryFormatDataTypeMap=DataTypeArray();
    DataTypeArray hdf5FormatDataTypeMap=DataTypeArray();
    DataTypeArray netcdfFormatDataTypeMap=DataTypeArray();
    DataTypeArray csvFormatDataTypeMap=DataTypeArray();
    DataTypeArray jsonFormatDataTypeMap=DataTypeArray();
    DataTypeArray xmlFormatDataTypeMap=DataTypeArray();
    DataTypeArray avroFormatDataTypeMap=DataTypeArray();
    DataTypeArray parquetFormatDataTypeMap=DataTypeArray();
    /* I/O type: 1_0_0: Compression Speed */
    /* DataFormat: 1: dummy */
    dummyFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: binary */
    binaryFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::HUFF;
    binaryFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::QLZ;
    binaryFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::QLZ;
    binaryFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;
    /* DataFormat: 1: hdf5 */
    hdf5FormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::HUFF;
    hdf5FormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::QLZ;
    hdf5FormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::QLZ;
    hdf5FormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::QLZ;
    hdf5FormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::QLZ;
    /* DataFormat: 1: netcdf */
    netcdfFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::HUFF;
    netcdfFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::QLZ;
    netcdfFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::QLZ;
    netcdfFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::QLZ;
    netcdfFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;
    /* DataFormat: 1: csv */
    csvFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::LZO;
    csvFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RLE;
    csvFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RLE;
    csvFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RLE;
    csvFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RLE;
    csvFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RLE;
    csvFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RLE;
    /* DataFormat: 1: json */
    jsonFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RLE;
    jsonFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::LZO;
    jsonFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::LZO;
    jsonFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::LZO;
    jsonFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::LZO;
    jsonFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RLE;
    jsonFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;
    /* DataFormat: 1: xml */
    xmlFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::LZO;
    xmlFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RLE;
    xmlFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RLE;
    xmlFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RLE;
    xmlFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RLE;
    xmlFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::LZO;
    xmlFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;
    /* DataFormat: 1: avro */
    avroFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::LZO;
    avroFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RLE;
    avroFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RLE;
    avroFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RLE;
    avroFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RLE;
    avroFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::LZO;
    avroFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;
    /* DataFormat: 1: parquet */
    parquetFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::LZO;
    parquetFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RLE;
    parquetFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RLE;
    parquetFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RLE;
    parquetFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RLE;
    parquetFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::LZO;
    parquetFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;

    dataFormatMap[as_integer(DataFormat::DUMMY)]=dummyFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::BINARY)]=binaryFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::HDF5)]=hdf5FormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::NETCDF)]=netcdfFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::CSV)]=csvFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::JSON)]=jsonFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::XML)]=xmlFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::AVRO)]=avroFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::PARQUET)]=parquetFormatDataTypeMap;
    right_lib[as_integer(PriorityType::W_1_0_0)]=dataFormatMap;

    /* I/O type: 0_1_0: decompression Speed */
    /* DataFormat: 1: dummy */
    dummyFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: binary */
    binaryFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::HUFF;
    binaryFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::ZLIB;
    binaryFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::ZLIB;
    binaryFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::HUFF;
    binaryFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;
    /* DataFormat: 1: hdf5 */
    hdf5FormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::HUFF;
    hdf5FormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::ZLIB;
    hdf5FormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::HUFF;
    hdf5FormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;
    /* DataFormat: 1: netcdf */
    netcdfFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::HUFF;
    netcdfFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::QLZ;
    netcdfFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::QLZ;
    netcdfFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::QLZ;
    netcdfFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::HUFF;
    netcdfFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;
    /* DataFormat: 1: csv */
    csvFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::HUFF;
    csvFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::HUFF;
    csvFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::HUFF;
    csvFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::HUFF;
    csvFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::HUFF;
    csvFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::HUFF;
    csvFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::HUFF;
    /* DataFormat: 1: json */
    jsonFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::HUFF;
    jsonFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::HUFF;
    jsonFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::HUFF;
    jsonFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::HUFF;
    jsonFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::HUFF;
    jsonFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RLE;
    jsonFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::HUFF;
    /* DataFormat: 1: xml */
    xmlFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::HUFF;
    xmlFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::HUFF;
    xmlFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::HUFF;
    xmlFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::HUFF;
    xmlFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::HUFF;
    xmlFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::HUFF;
    xmlFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::HUFF;
    /* DataFormat: 1: avro */
    avroFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::HUFF;
    avroFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::LZO;
    avroFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::BZ2;
    avroFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::HUFF;
    avroFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::HUFF;
    avroFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::HUFF;
    avroFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;
    /* DataFormat: 1: parquet */
    parquetFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::HUFF;
    parquetFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::ZLIB;
    parquetFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::HUFF;
    parquetFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::HUFF;
    parquetFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::HUFF;
    parquetFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;

    dataFormatMap[as_integer(DataFormat::DUMMY)]=dummyFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::BINARY)]=binaryFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::HDF5)]=hdf5FormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::NETCDF)]=netcdfFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::CSV)]=csvFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::JSON)]=jsonFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::XML)]=xmlFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::AVRO)]=avroFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::PARQUET)]=parquetFormatDataTypeMap;
    right_lib[as_integer(PriorityType::W_0_1_0)]=dataFormatMap;

    /* I/O type: 0_0_1: compression ratio */
    /* DataFormat: 1: dummy */
    dummyFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: binary */
    binaryFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: hdf5 */
    hdf5FormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: netcdf */
    netcdfFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: csv */
    csvFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: json */
    jsonFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::SF;
    jsonFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: xml */
    xmlFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: avro */
    avroFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: parquet */
    parquetFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;

    dataFormatMap[as_integer(DataFormat::DUMMY)]=dummyFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::BINARY)]=binaryFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::HDF5)]=hdf5FormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::NETCDF)]=netcdfFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::CSV)]=csvFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::JSON)]=jsonFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::XML)]=xmlFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::AVRO)]=avroFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::PARQUET)]=parquetFormatDataTypeMap;
    right_lib[as_integer(PriorityType::W_0_0_1)]=dataFormatMap;


    /* I/O type: 1_1_0: compression and decompression speed */
    /* DataFormat: 1: dummy */
    dummyFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: binary */
    binaryFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::HUFF;
    binaryFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::QLZ;
    binaryFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::QLZ;
    binaryFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::HUFF;
    binaryFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;
    /* DataFormat: 1: hdf5 */
    hdf5FormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::HUFF;
    hdf5FormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::QLZ;
    hdf5FormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::QLZ;
    hdf5FormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::QLZ;
    hdf5FormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;
    /* DataFormat: 1: netcdf */
    netcdfFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::HUFF;
    netcdfFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::QLZ;
    netcdfFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::QLZ;
    netcdfFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::QLZ;
    netcdfFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::HUFF;
    netcdfFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;
    /* DataFormat: 1: csv */
    csvFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::LZO;
    csvFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RLE;
    csvFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RLE;
    csvFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RLE;
    csvFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RLE;
    csvFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RLE;
    csvFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::QLZ;
    /* DataFormat: 1: json */
    jsonFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RLE;
    jsonFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::LZO;
    jsonFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::LZO;
    jsonFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::LZO;
    jsonFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::LZO;
    jsonFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RLE;
    jsonFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;
    /* DataFormat: 1: xml */
    xmlFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::TRLE;
    xmlFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RLE;
    xmlFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RLE;
    xmlFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RLE;
    xmlFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RLE;
    xmlFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::LZO;
    xmlFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;
    /* DataFormat: 1: avro */
    avroFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::LZO;
    avroFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::LZO;
    avroFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::LZO;
    avroFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::QLZ;
    avroFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::HUFF;
    avroFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;
    /* DataFormat: 1: parquet */
    parquetFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::HUFF;
    parquetFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::QLZ;
    parquetFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::LZO;
    parquetFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::LZO;
    parquetFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::HUFF;
    parquetFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::LZO;

    dataFormatMap[as_integer(DataFormat::DUMMY)]=dummyFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::BINARY)]=binaryFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::HDF5)]=hdf5FormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::NETCDF)]=netcdfFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::CSV)]=csvFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::JSON)]=jsonFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::XML)]=xmlFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::AVRO)]=avroFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::PARQUET)]=parquetFormatDataTypeMap;
    right_lib[as_integer(PriorityType::W_1_1_0)]=dataFormatMap;

    /* I/O type: 0_1_1: decompression speed and compression ratio */
    /* DataFormat: 1: dummy */
    dummyFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: binary */
    binaryFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: hdf5 */
    hdf5FormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::QLZ;
    hdf5FormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: netcdf */
    netcdfFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::QLZ;
    netcdfFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: csv */
    csvFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::QLZ;
    /* DataFormat: 1: json */
    jsonFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::SF;
    jsonFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::SF;
    jsonFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: xml */
    xmlFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: avro */
    avroFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::QLZ;
    avroFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::QLZ;
    avroFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: parquet */
    parquetFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;

    dataFormatMap[as_integer(DataFormat::DUMMY)]=dummyFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::BINARY)]=binaryFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::HDF5)]=hdf5FormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::NETCDF)]=netcdfFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::CSV)]=csvFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::JSON)]=jsonFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::XML)]=xmlFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::AVRO)]=avroFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::PARQUET)]=parquetFormatDataTypeMap;
    right_lib[as_integer(PriorityType::W_0_1_1)]=dataFormatMap;

    /* I/O type: 1_0_1: compression speed and compression ratio */
    /* DataFormat: 1: dummy */
    dummyFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: binary */
    binaryFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: hdf5 */
    hdf5FormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::QLZ;
    hdf5FormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: netcdf */
    netcdfFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::QLZ;
    netcdfFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: csv */
    csvFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::QLZ;
    /* DataFormat: 1: json */
    jsonFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::SF;
    jsonFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::SF;
    jsonFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: xml */
    xmlFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: avro */
    avroFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::QLZ;
    avroFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::QLZ;
    avroFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: parquet */
    parquetFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;

    dataFormatMap[as_integer(DataFormat::DUMMY)]=dummyFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::BINARY)]=binaryFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::HDF5)]=hdf5FormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::NETCDF)]=netcdfFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::CSV)]=csvFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::JSON)]=jsonFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::XML)]=xmlFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::AVRO)]=avroFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::PARQUET)]=parquetFormatDataTypeMap;
    right_lib[as_integer(PriorityType::W_1_0_1)]=dataFormatMap;

    /* I/O type: 1_1_1: compression, decompression speed and compression ratio */
    /* DataFormat: 1: dummy */
    dummyFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    dummyFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: binary */
    binaryFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    binaryFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: hdf5 */
    hdf5FormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    hdf5FormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::QLZ;
    hdf5FormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: netcdf */
    netcdfFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::QLZ;
    netcdfFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    netcdfFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: csv */
    csvFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    csvFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::QLZ;
    /* DataFormat: 1: json */
    jsonFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::SF;
    jsonFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    jsonFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::SF;
    jsonFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: xml */
    xmlFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    xmlFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: avro */
    avroFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::QLZ;
    avroFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::QLZ;
    avroFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    avroFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;
    /* DataFormat: 1: parquet */
    parquetFormatDataTypeMap[DataType::DUMMY->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::CHAR->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::SINT_SORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::SINT_UNSORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::UINT_SORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::UINT_UNSORTED->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::FLOAT->index_]=CompressionLibrary::RICE;
    parquetFormatDataTypeMap[DataType::DOUBLE->index_]=CompressionLibrary::RICE;

    dataFormatMap[as_integer(DataFormat::DUMMY)]=dummyFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::BINARY)]=binaryFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::HDF5)]=hdf5FormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::NETCDF)]=netcdfFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::CSV)]=csvFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::JSON)]=jsonFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::XML)]=xmlFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::AVRO)]=avroFormatDataTypeMap;
    dataFormatMap[as_integer(DataFormat::PARQUET)]=parquetFormatDataTypeMap;
    right_lib[as_integer(PriorityType::W_1_1_1)]=dataFormatMap;


}




//****************************** EOF *********************************//

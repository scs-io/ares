/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/13/17.
//

#ifndef ARES_ENGINE_H
#define ARES_ENGINE_H

#include <unordered_map>
#include <mutex>
#include <memory>
#include <string>
#include "../common/enumerations.h"
#include "../common/data_structure.h"

using std::string;

class Engine
{
public:
    Engine(); //Default Ctor
    bool predict(EngineInput input,EngineOutput &output);
private:
    PriorityType curr_perf_wt;
    typedef std::array<CompressionLibrary,8> DataTypeArray;
    typedef std::array<DataTypeArray,11> DataFormatArray;
    std::array<DataFormatArray,8> right_lib;
};


#endif //ARES_ENGINE_H

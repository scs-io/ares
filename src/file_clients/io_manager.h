/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 3/17/18.
//

#ifndef ARES_IO_MGR_H
#define ARES_IO_MGR_H


#include <string>
#include "../common/enumerations.h"



class IOManager{
public:
    IOManager() = default; //Default Ctor
    ~IOManager() = default; //Dtor
    IOManager(const IOManager&) = delete; //No Copy Ctor
    IOManager& operator=(const IOManager&) = delete; //No Assignment Optr
    IOManager(IOManager &&) = delete; // No Move Ctor
    IOManager& operator=(IOManager&&) = delete; //No Move Assignment Optr

    bool write(std::string filename,void*&buffer,size_t &size,DataFormat format,bool op);
    bool read(std::string filename,void*&buffer,size_t &size);
    std::string DeduceFilename(const std::string &file_path, bool op_flag);
private:
    size_t GetFileSize(const std::string &file_path);


};


#endif //ARES_IO_MGR_H

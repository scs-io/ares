/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 9/18/18.
//

#include <iostream>
#include <cstring>
#include "formatter.h"
#include "../common/debug.h"
#include "../common/error_codes.h"

bool Formatter::encode(void *&buffer, size_t &buffer_size, Metadata &metadata) {
    //Check for NULL input buffer
    DEBUG("encode"+std::to_string(buffer_size-metadata.GetSize())+" "+std::to_string(metadata.GetSize()));
    if(buffer == nullptr){
        DEBUG("IOManager::add_meta_data(): Input buffer is NULL!");
        return FAILURE;
    }

    //Allocate a new buffer to hold both the meta data (as prefix) and the buffer contents
    //This operation is expensive, but is applied only for buffer to buffer compression cases.
    void* buffer2 = malloc(buffer_size* sizeof(char));
    if(nullptr == buffer2){
        DEBUG("IOManager::add_meta_data(): Can't allocate memory of (file) size: " << metadata.GetSize() + buffer_size << " bytes! Exiting...");
        return FAILURE;
    }

    //Do the required copies
    memcpy(buffer2, metadata.encode(), metadata.GetSize());
    memcpy(buffer2 + metadata.GetSize(), buffer, buffer_size-metadata.GetSize());

    //Release the input buffer (old contents)
    free(buffer);
    buffer = nullptr;

    //Re-assign the input buffer pointer to the newly created buffer
    buffer = buffer2;
    return SUCCESS;
}

bool Formatter::decode(const void *buffer, size_t &buffer_size, Metadata &metadata) {
    //Check for NULL input buffer
    if(buffer == nullptr){
        DEBUG("IOManager::read_meta_data(): Input buffer is NULL!");
        return FAILURE;
    }

    //Copy the required values from the buffer and fill the meta data structure
    metadata.decode(buffer);

    DEBUG("decode done"+std::to_string(metadata.uncompressedSize)+" "+std::to_string(metadata.compressedSize)+" "+std::to_string(metadata.compressionUnits.size()));
    //Check if the meta data exists or isn't corrupted
    if(metadata.uncompressedSize == 0 || metadata.compressedSize == 0 || metadata.compressionUnits.empty())
    {
        DEBUG("IOManager::read_meta_data(): Meta-data is corrupted in the given .ares file!");
        return FAILURE;
    }
    return SUCCESS;
}

/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 9/18/18.
//

#ifndef ARES_ARES_FORMATTER_H
#define ARES_ARES_FORMATTER_H


#include <cstdio>
#include "../common/data_structure.h"

class Formatter {
public:
    Formatter() = default; //Default Ctor
    ~Formatter() = default; //Dtor
    Formatter(const Formatter&) = delete; //No Copy Ctor
    Formatter& operator=(const Formatter&) = delete; //No Assignment Optr
    Formatter(Formatter &&) = delete; // No Move Ctor
    Formatter& operator=(Formatter&&) = delete; //No Move Assignment Optr
    /**
     * public functions
     */
    bool encode(void *&buffer, size_t &buffer_size,Metadata &metadata);
    bool decode(const void *buffer, size_t &buffer_size,Metadata &metadata);
};


#endif //ARES_ARES_FORMATTER_H

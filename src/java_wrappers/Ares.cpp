/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 10/11/18.
//

#include <iostream>
#include <cstring>
#include "Ares.h"
#include "../../include/c++/ares.h"

jbyteArray Java_Ares_c_1compress(JNIEnv *env, jclass, jbyteArray source, jint source_size, jshort data_type, jshort library, jshort format, jshort priority) {

    void* source_ptr=malloc(source_size);
    env->GetByteArrayRegion(source, 0, source_size, (jbyte *)source_ptr);                                              
    void* destination_ptr;
    size_t source_size_v=source_size;
    size_t destination_size_v2;

    std::vector<CompressionUnit> compressionUnits=std::vector<CompressionUnit>();
    compressionUnits.emplace_back(CompressionUnit(0,source_size,DataType(data_type),CompressionLibrary (library)));
    AresFlags flags=AresFlags(CompressionLibrary (library),DataFormat (format),compressionUnits,PriorityType (priority));
    
    bool status= ares_compress( source_ptr, source_size_v, destination_ptr, destination_size_v2, flags);
    char* temp = static_cast<char*> (destination_ptr);
    jbyteArray destination=env->NewByteArray(destination_size_v2);
    env->SetByteArrayRegion(destination, 0, destination_size_v2, (jbyte *)destination_ptr);
    std::cout<<"\nCompression Done in c "<<destination_size_v2<<"\n";
    free(destination_ptr);
    free(source_ptr);
    return destination;
}

jbyteArray Java_Ares_c_1decompress(JNIEnv *env, jclass, jbyteArray source, jint source_size, jshort data_type, jshort library, jshort format, jshort priority) {
    source_size=env->GetArrayLength(source);
    void* source_ptr=malloc(source_size);
    env->GetByteArrayRegion(source, 0, source_size, (jbyte *)source_ptr);  
    void* destination_ptr=nullptr;
    size_t source_size_v=source_size;
    size_t destination_size_v2=0;

    std::vector<CompressionUnit> compressionUnits=std::vector<CompressionUnit>();
    compressionUnits.emplace_back(CompressionUnit(0,source_size,DataType(data_type),CompressionLibrary (library)));
    AresFlags flags=AresFlags(CompressionLibrary (library),DataFormat (format),compressionUnits,PriorityType (priority));
    
    bool status= ares_decompress( source_ptr, source_size_v, destination_ptr, destination_size_v2, flags);
    jbyteArray destination;
    
        destination=env->NewByteArray(destination_size_v2);
        env->SetByteArrayRegion(destination, 0, destination_size_v2, (jbyte *)destination_ptr);
    
    std::cout<<"\nCompression Done in c "<<destination_size_v2<<"\n";
    free(source_ptr);
    free(destination_ptr);
    return destination;
}

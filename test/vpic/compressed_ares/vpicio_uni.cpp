// Description: This is a simple benchmark based on VPIC's I/O interface
//		Each process writes a specified number of particles into 
//		a h5part output file
// Author:	Suren Byna <SByna@lbl.gov>
//		Lawrence Berkeley National Laboratory, Berkeley, CA
// 


#include "H5Part.h"
#include "H5Block.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// A simple timer based on gettimeofday
#include "./timer.h"
#include "../../../include/c++/ares.h"

struct timeval start_time[3];
float elapse[3];

H5PartFile* file;
int numparticles = 8388608;	// 8  meg particles per process
float *x, *y, *z;
float *px, *py, *pz;
h5part_int32_t *id1, *id2;
int x_dim = 64;
int y_dim = 64; 
int z_dim = 64;

// Uniform random number
inline double uniform_random_number() 
{
	return (((double)rand())/((double)(100)));
}

// Initialize particle data
void init_particles ()
{
	int i;
	for (i=0; i<numparticles; i++) 
	{
		id1[i] = i;
		id2[i] = i*2;
		x[i] = uniform_random_number()*x_dim;
		y[i] = uniform_random_number()*y_dim;
		z[i] = ((double)id1[i]/numparticles)*z_dim;    
		px[i] = uniform_random_number()*x_dim;
		py[i] = uniform_random_number()*y_dim;
		pz[i] = ((double)id2[i]/numparticles)*z_dim;    
	}
}

// Create H5Part file and write data
void create_and_write_synthetic_h5part_data(int rank) 
{
	// Note: printf statements are inserted basically 
	// to check the progress. Other than that they can be removed
	H5PartSetStep(file, 0); // only 1 timestep in this file
	if (rank == 0) printf ("Step is set \n");
	H5PartSetNumParticles(file, numparticles);
	if (rank == 0) printf ("Num particles is set \n");
	float var_data_size;
    var_data_size = numparticles * sizeof(h5part_float32_t);
    void* destination;
    const void* source_ptr= reinterpret_cast<char *>(x);
    size_t destination_size;
	AresFlags flags=AresFlags();
    flags.format_=DataFormat::BINARY;
	flags.priority=PriorityType::W_1_0_0;
	flags.library=CompressionLibrary::RICE;
	flags.compressionUnits.emplace_back(CompressionUnit(0,var_data_size,DataType::FLOAT,CompressionLibrary::ARES));
    ares_compress(source_ptr,(size_t)var_data_size,destination,destination_size,flags);
	H5PartSetNumParticles(file, destination_size/ sizeof(int));
	timer_on (2);
	H5PartWriteDataInt32(file,"x",(int*)destination);
	timer_off (2);
    var_data_size = numparticles * sizeof(h5part_float32_t)/(1024*1024);
    float comp_var_data_size = destination_size/(1024*1024);

#ifdef TIME_DEBUG
	printf ("Rank: %d, wrote compressed %.2f MB of %.2f MB, time: %f sec; rate: %f\n", rank, comp_var_data_size, var_data_size, elapse[2], comp_var_data_size/elapse[2]);
#endif
	timer_reset(2);
	free(destination);
	if (rank == 0) printf ("Written variable 1 \n");
    source_ptr=y;
    var_data_size = numparticles * sizeof (h5part_float32_t);
    ares_compress(source_ptr,(size_t)var_data_size,destination,destination_size,flags);
	H5PartSetNumParticles(file, destination_size/ sizeof(int));
	timer_on (2);
	H5PartWriteDataInt32(file,"y",(int*)destination);

	timer_off (2);
	var_data_size = numparticles * sizeof (h5part_float32_t)/(1024*1024);
    comp_var_data_size = destination_size/(1024*1024);
#ifdef TIME_DEBUG
    printf ("Rank: %d, wrote compressed %.2f MB of %.2f MB, time: %f sec; rate: %f\n", rank, comp_var_data_size, var_data_size, elapse[2], comp_var_data_size/elapse[2]);
#endif
	timer_reset(2);
    if (rank == 0) printf ("Written variable 2 \n");
    free(destination);
    source_ptr=z;
    var_data_size = numparticles * sizeof (h5part_float32_t);
    ares_compress(source_ptr,(size_t)var_data_size,destination,destination_size,flags);
	H5PartSetNumParticles(file, destination_size/ sizeof(int));
	timer_on (2);
	H5PartWriteDataInt32(file,"z",(int*)destination);
	timer_off (2);
	var_data_size = numparticles * sizeof (h5part_float32_t)/(1024*1024);
    comp_var_data_size = destination_size/(1024*1024);
#ifdef TIME_DEBUG
    printf ("Rank: %d, wrote compressed %.2f MB of %.2f MB, time: %f sec; rate: %f\n", rank, comp_var_data_size, var_data_size, elapse[2], comp_var_data_size/elapse[2]);
#endif
	timer_reset(2);
    if (rank == 0) printf ("Written variable 3 \n");
    free(destination);
    source_ptr=id1;
    var_data_size = numparticles * sizeof (h5part_int32_t);
	flags.compressionUnits[0].dataType=DataType::SINT_UNSORTED;
	
    ares_compress(source_ptr,(size_t)var_data_size,destination,destination_size,flags);
	H5PartSetNumParticles(file, destination_size/ sizeof(int));
	timer_on (2);
	H5PartWriteDataInt32(file,"id1",(int*)destination);
	timer_off (2);
	var_data_size = numparticles * sizeof (h5part_int32_t)/(1024*1024);
    comp_var_data_size = destination_size/(1024*1024);
#ifdef TIME_DEBUG
    printf ("Rank: %d, wrote compressed %.2f MB of %.2f MB, time: %f sec; rate: %f\n", rank, comp_var_data_size, var_data_size, elapse[2], comp_var_data_size/elapse[2]);
#endif
	timer_reset(2);
    if (rank == 0) printf ("Written variable 4 \n");
    free(destination);
    source_ptr=id2;
    var_data_size = numparticles * sizeof (h5part_int32_t);
    ares_compress(source_ptr,(size_t)var_data_size,destination,destination_size,flags);
	H5PartSetNumParticles(file, destination_size/ sizeof(int));
	timer_on (2);
	H5PartWriteDataInt32(file,"id2",(int*)destination);

	timer_off (2);
	var_data_size = numparticles * sizeof (h5part_int32_t)/(1024*1024);
    comp_var_data_size = destination_size/(1024*1024);
#ifdef TIME_DEBUG
    printf ("Rank: %d, wrote compressed %.2f MB of %.2f MB, time: %f sec; rate: %f\n", rank, comp_var_data_size, var_data_size, elapse[2], comp_var_data_size/elapse[2]);
#endif
	timer_reset(2);
    if (rank == 0) printf ("Written variable 5 \n");
    free(destination);
    source_ptr=px;
    var_data_size = numparticles * sizeof (h5part_float32_t);
	flags.compressionUnits[0].dataType=DataType::FLOAT;
    ares_compress(source_ptr,(size_t)var_data_size,destination,destination_size,flags);
	H5PartSetNumParticles(file, destination_size/ sizeof(int));
	timer_on (2);
	H5PartWriteDataInt32(file,"px",(int*)destination);
	timer_off (2);
	var_data_size = numparticles * sizeof (h5part_float32_t)/(1024*1024);
    comp_var_data_size = destination_size/(1024*1024);
#ifdef TIME_DEBUG
    printf ("Rank: %d, wrote compressed %.2f MB of %.2f MB, time: %f sec; rate: %f\n", rank, comp_var_data_size, var_data_size, elapse[2], comp_var_data_size/elapse[2]);
#endif
	timer_reset(2);
    if (rank == 0) printf ("Written variable 6 \n");
    free(destination);
    source_ptr=py;
    var_data_size = numparticles * sizeof (h5part_float32_t);
    ares_compress(source_ptr,(size_t)var_data_size,destination,destination_size,flags);
	H5PartSetNumParticles(file, destination_size/ sizeof(int));
	timer_on (2);
	H5PartWriteDataInt32(file,"py",(int*)destination);
	timer_off (2);
	var_data_size = numparticles * sizeof (h5part_float32_t)/(1024*1024);
    comp_var_data_size = destination_size/(1024*1024);
#ifdef TIME_DEBUG
    printf ("Rank: %d, wrote compressed %.2f MB of %.2f MB, time: %f sec; rate: %f\n", rank, comp_var_data_size, var_data_size, elapse[2], comp_var_data_size/elapse[2]);
#endif
	timer_reset(2);
    if (rank == 0) printf ("Written variable 7 \n");
    free(destination);
    source_ptr=pz;
    var_data_size = numparticles * sizeof (h5part_float32_t);
    ares_compress(source_ptr,(size_t)var_data_size,destination,destination_size,flags);
	H5PartSetNumParticles(file, destination_size/ sizeof(int));
	timer_on (2);
	H5PartWriteDataInt32(file,"pz",(int*)destination);
	timer_off (2);
	var_data_size = numparticles * sizeof (h5part_float32_t)/(1024*1024);
    comp_var_data_size = destination_size/(1024*1024);
#ifdef TIME_DEBUG
    printf ("Rank: %d, wrote compressed %.2f MB of %.2f MB, time: %f sec; rate: %f\n", rank, comp_var_data_size, var_data_size, elapse[2], comp_var_data_size/elapse[2]);
#endif
	timer_reset(2);
    if (rank == 0) printf ("Written variable 8 \n");
    free(destination);
}

int main (int argc, char* argv[]) 
{
	char *file_name = argv[1];
	
	MPI_Init(&argc,&argv);
	int my_rank, num_procs;
	MPI_Comm_rank (MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size (MPI_COMM_WORLD, &num_procs);

	if (argc == 3)
	{
		numparticles = (atoi (argv[2]))*1024*1024;
	}
	else
	{
		numparticles = 1*1024*1024;
	}

	if (my_rank == 0) {printf ("Number of paritcles: %ld \n", numparticles);}

	x=(float*)malloc(numparticles*sizeof(double));
	y=(float*)malloc(numparticles*sizeof(double));
	z=(float*)malloc(numparticles*sizeof(double));

	px=(float*)malloc(numparticles*sizeof(double));
	py=(float*)malloc(numparticles*sizeof(double));
	pz=(float*)malloc(numparticles*sizeof(double));

	id1=(h5part_int32_t*)malloc(numparticles*sizeof(h5part_int32_t));
	id2=(h5part_int32_t*)malloc(numparticles*sizeof(h5part_int32_t));

	init_particles ();

	if (my_rank == 0)
	{
		printf ("Finished initializing particles \n");
	}

	MPI_Barrier (MPI_COMM_WORLD);
	timer_on (0);
	//file = H5PartOpenFileParallelAlign(file_name, H5PART_WRITE, MPI_COMM_WORLD, alignf);
	// file = H5PartOpenFileParallel (file_name, H5PART_WRITE | H5PART_VFD_MPIPOSIX | H5PART_FS_LUSTRE, MPI_COMM_WORLD);
	file = H5PartOpenFileParallel (file_name, H5PART_WRITE | H5PART_FS_LUSTRE, MPI_COMM_WORLD);

	if (my_rank == 0)
	{
		printf ("Opened H5Part file... \n");
	}
	// Throttle and see performance
	// H5PartSetThrottle (file, 10);

	H5PartWriteFileAttribString(file, "Origin", "Tested by Suren");

	MPI_Barrier (MPI_COMM_WORLD);
	timer_on (1);

	if (my_rank == 0) printf ("Before writing particles \n");
	create_and_write_synthetic_h5part_data(my_rank);

	MPI_Barrier (MPI_COMM_WORLD);
	timer_off (1);
	if (my_rank == 0) printf ("After writing particles \n");

	H5PartCloseFile(file);
	if (my_rank == 0) printf ("After closing particles \n");

	free(x); free(y); free(z);
	free(px); free(py); free(pz);
	free(id1);
	free(id2);

	MPI_Barrier (MPI_COMM_WORLD);

	timer_off (0);

	if (my_rank == 0)
	{
		printf ("\nTiming results\n");
		timer_msg (1, "just writing data");
		timer_msg (0, "opening, writing, closing file");
		printf ("\n");
	}

	MPI_Finalize();

	return 0;
}

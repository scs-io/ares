/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by umashankar on 12/14/17.
//

//DRIVER src_buffer CODE
//The Engine works with any allocated src_buffer, provided its size and content type are known.
//For testing purpose, here user inputs file data along with its type as cmd-line arguments.
//The file is converted/split into src_buffer/s and supplied to the engine.

#include <iostream>
#include <string>

#include <cstdio>
#include <string>
#include <sys/stat.h>
#include <getopt.h>

#include "../include/c++/ares.h"
#include "../src/common/enumerations.h"
#include "../src/common/error_codes.h"
#include "common/data_structure.h"
#include "common/utils.h"

static bool GetFileExtension(const std::string &s, std::string &ext) {
    size_t i = s.rfind('.', s.length());
    if (i != std::string::npos) {
        ext=(s.substr(i+1, s.length() - i));
        return SUCCESS;
    }
    return FAILURE;
}

//---------------------- MAIN --------------------------
int main(int argc, char* argv[])
{
    InputArgs args;
    if(!parse_opts(argc,argv,args)){
        return FAILURE;
    }
    //---------------------- FILE INPUT -------------------------------
    AresFlags flags;
    flags.format_=args.format;
    flags.library=CompressionLibrary(args.library);
    flags.priority=PriorityType(args.priority);
    if(flags.format_==DataFormat::DUMMY){
        std::string ext;
        if(GetFileExtension(args.filepath, ext)){
            if(ext=="avro"){
                flags.format_=DataFormat::AVRO;
            }else if(ext=="bin"){
                flags.format_=DataFormat::BINARY;
            }else if(ext=="csv"){
                flags.format_=DataFormat::CSV;
            }else if(ext=="h5"){
                flags.format_=DataFormat::HDF5;
            }else if(ext=="json"){
                flags.format_=DataFormat::JSON;
            }else if(ext=="dat"){
                flags.format_=DataFormat::MPIIO;
            }else if(ext=="nc"){
                flags.format_=DataFormat::NETCDF;
            }else if(ext=="parquet"){
                flags.format_=DataFormat::PARQUET;
            }else if(ext=="xml"){
                flags.format_=DataFormat::XML;
            }
        }
    }
        //Compress file:

        if(ares_compress(args.filepath, flags)) {
            std::cout << "File Compression Success!" << std::endl;
        }
        else{
            std::cout << "File Compression Failed!" << std::endl;
        }
        //Decompress file:
        if (ares_decompress(args.filepath+".ares",flags)) {
            std::cout << "File Decompression Success!" << std::endl;
        }
        else{
            std::cout << "File Decompression Failed!" << std::endl;
        }
    return 1;
}

//--------------------------------------------------------------------


//****************************** EOF *********************************

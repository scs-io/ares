/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan
 * Devarajan <hdevarajan@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Ares
 * 
 * Ares is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 9/19/18.
//

#ifndef ARES_DATA_STRUCTURE_2_H
#define ARES_DATA_STRUCTURE_2_H

#include <string>

typedef struct InputArgs{
    std::string filepath;
    CompressionLibrary library;
    DataFormat format;
    PriorityType priority;
    DataType dataType;
    bool compress;
    InputArgs():filepath(),
                compress(true),
                library(CompressionLibrary::DUMMY),
                format(DataFormat::DUMMY),
                priority(PriorityType::W_1_1_1){}
} InputArgs;
#endif //ARES_DATA_STRUCTURE_2_H
